package com.urwasoft.icon.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.urwasoft.icon.R;
import com.urwasoft.icon.fragment.PhotosFragment;
import com.urwasoft.icon.util.Utils;

public class PhotosActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new PhotosFragment();
    }

    @Override
    protected int getLayout() {
        return R.layout.single_fragment_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getTxtScreenTitle().setText(new Utils(this).getStringFromResourceId(R.string.add_photos));
        getImgHomeLogo().setVisibility(View.VISIBLE);
        getImgHomeLogo().setImageResource(R.drawable.btn_back);
        getImgHomeLogo().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    public void onBackPressed(Intent intent){
        this.onBackPressed();
    }
}
