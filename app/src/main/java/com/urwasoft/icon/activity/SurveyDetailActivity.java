package com.urwasoft.icon.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.urwasoft.icon.R;
import com.urwasoft.icon.fragment.SurveyDetailFragment;
import com.urwasoft.icon.util.SessionManager;
import com.urwasoft.icon.util.Utils;

public class SurveyDetailActivity extends SingleFragmentActivity {
    private Utils utils;

    public static final String IS_SURVEY_UPDATED = "com.urwasoft.icon.IS_SURVEY_UPDATED";
    public static final String SURVEY_RESPONSE = "com.urwasoft.icon.SURVEY_RESPONSE";

    @Override
    protected Fragment createFragment() {
        return new SurveyDetailFragment();
    }

    @Override
    protected int getLayout() {
        return R.layout.single_fragment_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        utils = new Utils(this);
        getTxtScreenTitle().setText(utils.getStringFromResourceId(R.string.survey_detail));
        getImgHomeLogo().setVisibility(View.VISIBLE);
        getImgHomeLogo().setImageResource(R.drawable.btn_back);
        getImgHomeLogo().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getTxtSignOut().setText(utils.getStringFromResourceId(R.string.sign_out));
        getTxtSignOut().setVisibility(View.VISIBLE);
        getTxtSignOut().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                SessionManager.get(SurveyDetailActivity.this).logoutUser();
            }
        });
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.fragmentContainer);
        if(fragment instanceof SurveyDetailFragment) {
            SurveyDetailFragment surveyDetailFragment = (SurveyDetailFragment) fragment;
            Intent intent = new Intent();
            intent.putExtra(SurveyDetailActivity.SURVEY_RESPONSE, surveyDetailFragment.surveyResponse);
            intent.putExtra(SurveyDetailActivity.IS_SURVEY_UPDATED, surveyDetailFragment.isSurveyUpdated);
            setResult(Activity.RESULT_OK, intent);
        }
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
}
















