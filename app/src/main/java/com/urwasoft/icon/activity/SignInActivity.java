package com.urwasoft.icon.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.urwasoft.icon.R;
import com.urwasoft.icon.fragment.SignInFragment;

public class SignInActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new SignInFragment();
    }

    @Override
    protected int getLayout() {
        return R.layout.single_fragment_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getToolbar().setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
}
