package com.urwasoft.icon.activity;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.urwasoft.icon.R;
import com.urwasoft.icon.fragment.NavigationDrawerFragment;
import com.urwasoft.icon.util.AppConstants;
import com.urwasoft.icon.util.SessionManager;

/**
 * Created by Shahrukh Malik on 2/19/2016.
 */
public abstract class SingleFragmentActivity extends AppCompatActivity{
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private ImageView imgHomeLogo,imgDone;
    private TextView txtScreenTitle,txtLogout;
    private CoordinatorLayout mainContent;
    private boolean isArabic;

    protected abstract Fragment createFragment();
    protected abstract int getLayout();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        initializeViews();
        setSupportActionBar(getToolbar());
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.fragmentContainer);

        if(fragment == null) {
            fragment = createFragment();
            manager.beginTransaction().add(R.id.fragmentContainer, fragment, "yourTag").commit();
        }

        overridePendingTransition(0,0);
        FacebookSdk.sdkInitialize(SingleFragmentActivity.this.getApplicationContext());
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        /*super.onPostCreate(savedInstanceState);
        isArabic = SessionManager.get(SingleFragmentActivity.this).isArabic();
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);*/
        super.onPostCreate(savedInstanceState);
        isArabic = SessionManager.get(SingleFragmentActivity.this).isArabic();
        setupNavigationDrawer();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        if (drawerLayout != null) {
            if (mainContent != null) {
                mainContent.setAlpha(0);
                mainContent.animate().alpha(1).setDuration(AppConstants.MAIN_CONTENT_FADEIN_DURATION);
            }
        }
        else {
            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        }
    }

    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/

    private void initializeViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mainContent = (CoordinatorLayout) findViewById(R.id.mainContent);
        imgHomeLogo = (ImageView) toolbar.findViewById(R.id.imgHomeLogo);
        imgDone = (ImageView) toolbar.findViewById(R.id.imgDone);
        txtScreenTitle = (TextView) toolbar.findViewById(R.id.txtScreenTitle);
        txtLogout = (TextView) toolbar.findViewById(R.id.txtLogout);

        imgHomeLogo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(SingleFragmentActivity.this,MenuActivity.class);
                startActivity(intent);*/
            }
        });
        imgDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void setupNavigationDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        if (drawerLayout == null) {
            return;
        }
        addNavigationDrawerFragment();
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, getToolbar(), R.string.open, R.string.close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();

            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        /*drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.syncState();
            }
        });*/

        imgHomeLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDrawerOpen()) {
                    closeDrawer();
                } else {
                    openDrawer();
                }
            }
        });

        /*navigationDrawer = (FrameLayout) findViewById(R.id.navigationDrawerFragment);
        int width = getResources().getDisplayMetrics().widthPixels;
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navigationDrawer.getLayoutParams();
        params.width = width;
        navigationDrawer.setLayoutParams(params);*/
    }

    private void addNavigationDrawerFragment() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.navigationDrawerFragment);

        if (fragment == null) {
            fragment = new NavigationDrawerFragment();
            manager.beginTransaction().add(R.id.navigationDrawerFragment, fragment).commit();
        }
    }

    public void openDrawer(){
        if (drawerLayout == null) {
            return;
        }
        if(!(isDrawerOpen())) {
            if(isArabic){
                drawerLayout.openDrawer(Gravity.RIGHT);
            }else {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        }
    }

    public void closeDrawer(){
        if (drawerLayout == null) {
            return;
        }
        if(isDrawerOpen()) {
            if(isArabic){
                drawerLayout.closeDrawer(Gravity.RIGHT);
            }else {
                drawerLayout.closeDrawer(Gravity.LEFT);
            }
        }
    }

    public boolean isDrawerOpen(){
        if (drawerLayout == null) {
            return false;
        }
        if(isArabic){
            return drawerLayout.isDrawerOpen(Gravity.RIGHT);
        }else {
            return drawerLayout.isDrawerOpen(Gravity.LEFT);
        }
    }



    protected Toolbar getToolbar() {
        return toolbar;
    }

    protected TextView getTxtScreenTitle(){
        return txtScreenTitle;
    }

    protected TextView getTxtSignOut(){
        return txtLogout;
    }

    protected ImageView getImgHomeLogo(){
        return imgHomeLogo;
    }

    protected ImageView getImgDone(){
        return imgDone;
    }

    public CoordinatorLayout getMainContent(){
        return mainContent;
    }

    @Override
    public void onBackPressed() {

        if(isDrawerOpen()){
            closeDrawer();
        }
        else {
            super.onBackPressed();
        }
    }
}
















