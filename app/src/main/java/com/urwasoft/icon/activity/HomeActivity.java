package com.urwasoft.icon.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.urwasoft.icon.R;
import com.urwasoft.icon.fragment.HomeFragment;
import com.urwasoft.icon.util.SessionManager;
import com.urwasoft.icon.util.Utils;

public class HomeActivity extends SingleFragmentActivity {
    private Utils utils;

    @Override
    protected Fragment createFragment() {
        return new HomeFragment();
    }

    @Override
    protected int getLayout() {
        return R.layout.single_fragment_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        utils = new Utils(this);
        getTxtScreenTitle().setText(utils.getStringFromResourceId(R.string.my_surveys));
        getTxtSignOut().setText(utils.getStringFromResourceId(R.string.sign_out));
        getTxtSignOut().setVisibility(View.VISIBLE);
        getTxtSignOut().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                SessionManager.get(HomeActivity.this).logoutUser();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
}
