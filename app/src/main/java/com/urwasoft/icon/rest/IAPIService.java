package com.urwasoft.icon.rest;


import com.urwasoft.icon.rest.request.FacebookSignInUpRequest;
import com.urwasoft.icon.rest.request.ForgotPasswordRequest;
import com.urwasoft.icon.rest.request.GCMTokenRequest;
import com.urwasoft.icon.rest.request.SignInRequest;
import com.urwasoft.icon.rest.request.SignUpRequest;
import com.urwasoft.icon.rest.request.SubmitRequest;
import com.urwasoft.icon.rest.response.APIResponse;
import com.urwasoft.icon.rest.response.ErrorResponse2;
import com.urwasoft.icon.rest.response.SurveyResponse;
import com.urwasoft.icon.rest.response.UserResponse;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Shahrukh Malik on 2/10/2016.
 */
public interface IAPIService {


    /*@GET("/getuserforsignin/{username}/{password}/{token1}/{token2}")
    Call<UserResponse> signIn(@Path("username") String username,@Path("password") String password,@Path("token1") String token1,@Path("token2") String token2);

    @GET("/getmyclaims/{user_id}/{token1}/{token2}")
    Call<List<SurveyResponse>> getClaims(@Path("user_id") String userId, @Path("token1") String token1, @Path("token2") String token2);*/

    @GET("/getuserforsignin/{username}/{password}/{token1}/{token2}")
    Call<UserResponse> signIn(@Path("username") String username,@Path("password") String password,@Path("token1") String token1,@Path("token2") String token2);

    @GET("/getmyclaims/{user_id}/{token1}/{token2}")
    Call<List<SurveyResponse>> getClaims(@Path("user_id") int userId, @Path("token1") String token1, @Path("token2") String token2);

    @POST("/updateSurvey/{survey_id}/{token1}/{token2}")
    Call<ErrorResponse2> submitTotalCost(@Path("survey_id") int surveyId, @Body SubmitRequest submitRequest, @Path("token1") String token1, @Path("token2") String token2);

    /*@POST("/updateclaimcost/{survey_id}/{total_cost}/{token1}/{token2}")
    Call<ErrorResponse2> submitTotalCost(@Path("survey_id") int surveyId, @Path("total_cost") double totalCost, @Path("token1") String token1, @Path("token2") String token2);*/

    @POST("/saveclaimpic/{user_id}/{token1}/{token2}")
    Call<ErrorResponse2> uploadImage(@Body RequestBody photo,@Path("user_id") int userId, @Path("token1") String token1, @Path("token2") String token2);








































    @Headers({
            "Authorization: Z29tYWxsLWFwcC1hbmRyb2lkOjY2ZmU3MzBmLWQ2M2UtNDI2OS04ZmJhLTkwMGYxMDY4ZDdmOQ==",
            "client-id: gomall-app-android"
    })
    @PUT("/drondemandbackend/public/index.php/user/signup")
    Call<APIResponse<UserResponse>> signUp(@Body SignUpRequest signUpRequest, @Query("lang") String locale);

    @Headers({
            "Authorization: Z29tYWxsLWFwcC1hbmRyb2lkOjY2ZmU3MzBmLWQ2M2UtNDI2OS04ZmJhLTkwMGYxMDY4ZDdmOQ==",
            "client-id: gomall-app-android"
    })
    @POST("/drondemandbackend/public/index.php/user/login")
    Call<APIResponse<UserResponse>> signIn(@Body SignInRequest signInRequest, @Query("lang") String locale);

    @Headers({
            "Authorization: Z29tYWxsLWFwcC1hbmRyb2lkOjY2ZmU3MzBmLWQ2M2UtNDI2OS04ZmJhLTkwMGYxMDY4ZDdmOQ==",
            "client-id: gomall-app-android"
    })
    @POST("/resturantappbackend/public/api/user/connect/facebook")
    Call<APIResponse<UserResponse>> signInUpWithFacebook(@Body FacebookSignInUpRequest facebookSignInUpRequest, @Query("lang") String locale);

    @Headers({
            "Authorization: Z29tYWxsLWFwcC1hbmRyb2lkOjY2ZmU3MzBmLWQ2M2UtNDI2OS04ZmJhLTkwMGYxMDY4ZDdmOQ==",
            "client-id: gomall-app-android"
    })
    @POST("/resturantappbackend/public/api/user/forget")
    Call<APIResponse<UserResponse>> forgotPassword(@Body ForgotPasswordRequest forgotPasswordRequest, @Query("lang") String locale);

    @POST("/drondemandbackend/public/index.php/user/gcm")
    Call<APIResponse<UserResponse>> updateGCMToken(@Body GCMTokenRequest gcmTokenRequest, @Query("lang") String locale);

}














