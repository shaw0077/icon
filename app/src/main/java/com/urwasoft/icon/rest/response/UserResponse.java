package com.urwasoft.icon.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shahrukh Malik on 10/14/2016.
 */
public class UserResponse {
    @SerializedName("idx")
    @Expose
    private Integer idx;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("empIdx")
    @Expose
    private Integer empIdx;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("departmentname")
    @Expose
    private String departmentname;

    /**
     *
     * @return
     * The idx
     */
    public Integer getIdx() {
        return idx;
    }

    /**
     *
     * @param idx
     * The idx
     */
    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     * The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     * The empIdx
     */
    public Integer getEmpIdx() {
        return empIdx;
    }

    /**
     *
     * @param empIdx
     * The empIdx
     */
    public void setEmpIdx(Integer empIdx) {
        this.empIdx = empIdx;
    }

    /**
     *
     * @return
     * The fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     *
     * @param fullName
     * The fullName
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The departmentname
     */
    public String getDepartmentname() {
        return departmentname;
    }

    /**
     *
     * @param departmentname
     * The departmentname
     */
    public void setDepartmentname(String departmentname) {
        this.departmentname = departmentname;
    }
}
