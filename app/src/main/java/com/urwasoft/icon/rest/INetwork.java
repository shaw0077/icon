package com.urwasoft.icon.rest;


import com.urwasoft.icon.rest.response.APIResponse;

import retrofit2.Response;

/**
 * Created by Shahrukh Malik on 2/10/2016.
 */
public interface INetwork<T> {
    void onResponse(Response<APIResponse<T>> response);
    void onError(Response<APIResponse<T>> response);
    void onNullResponse();
    void onFailure(Throwable t);
}
