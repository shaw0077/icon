package com.urwasoft.icon.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shahrukh Malik on 2/29/2016.
 */
public class ErrorResponse {
    @SerializedName("custom_code")
    @Expose
    private Integer customCode;
    @SerializedName("message")
    @Expose
    private List<String> message = new ArrayList<String>();

    /**
     *
     * @return
     * The customCode
     */
    public Integer getCustomCode() {
        return customCode;
    }

    /**
     *
     * @param customCode
     * The custom_code
     */
    public void setCustomCode(Integer customCode) {
        this.customCode = customCode;
    }

    /**
     *
     * @return
     * The message
     */
    public List<String> getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(List<String> message) {
        this.message = message;
    }
}
