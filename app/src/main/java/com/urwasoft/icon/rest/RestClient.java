package com.urwasoft.icon.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.urwasoft.icon.rest.request.AccessToken;
import com.urwasoft.icon.util.AppConstants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Shahrukh Malik on 4/4/2016.
 */
public class RestClient {
    private static RestClient restClient;
    private IAPIService apiService;
    private Retrofit retrofit;

    private RestClient()
    {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(2, TimeUnit.MINUTES);
        httpClient.readTimeout(2, TimeUnit.MINUTES);
        //httpClient.addInterceptor(logging);

        retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL_PRODUCTION)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        apiService = retrofit.create(IAPIService.class);
    }

    private RestClient(final AccessToken token)
    {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(2, TimeUnit.MINUTES);
        httpClient.readTimeout(2, TimeUnit.MINUTES);
        //httpClient.addInterceptor(logging);

        if (token != null) {
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Accept", "application/json")
                            .header("Authorization",
                                    token.getTokenType() + " " + token.getAccessToken())
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }

        retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL_PRODUCTION)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        apiService = retrofit.create(IAPIService.class);
    }

    public static void clearInstance(){
        RestClient.restClient = null;
    }

    public static RestClient get(AccessToken token)
    {
        if(restClient == null)
        {
            restClient = new RestClient(token);
        }
        return restClient;
    }

    public static RestClient get()
    {
        if(restClient == null)
        {
            restClient = new RestClient();
        }
        return restClient;
    }

    public IAPIService getApiService() {
        return apiService;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}

