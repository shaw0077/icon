package com.urwasoft.icon.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Shahrukh Malik on 2/12/2016.
 */
public class APIListResponse<T> {
    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("response")
    @Expose
    private List<T> response;

    @SerializedName("error")
    @Expose
    private ErrorResponse errorResponse;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<T> getResponse() {
        return response;
    }

    public void setResponse(List<T> response) {
        this.response = response;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }

    public void setError(ErrorResponse errorResponse) {
        this.errorResponse = errorResponse;
    }
}
