package com.urwasoft.icon.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 10/14/2016.
 */
public class FacebookSignInUpRequest {
    @SerializedName("access_token")
    @Expose
    private String accessToken;

    /**
     *
     * @return
     * The accessToken
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     *
     * @param accessToken
     * The access_token
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
