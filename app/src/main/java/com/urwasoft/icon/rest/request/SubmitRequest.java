package com.urwasoft.icon.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shahrukh Malik on 1/2/2017.
 */

public class SubmitRequest {
    @SerializedName("total_cost")
    @Expose
    private Double totalCost;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("reg_no")
    @Expose
    private String regNo;
    @SerializedName("repairer")
    @Expose
    private String repairer;
    @SerializedName("engine_number")
    @Expose
    private String engineNumber;
    @SerializedName("chasis_no")
    @Expose
    private String chasisNo;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("loss_no")
    @Expose
    private String lossNo;
    @SerializedName("survey_details")
    @Expose
    private String surveyDetails;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("body_observation")
    @Expose
    private String bodyObservation;
    @SerializedName("accessories")
    @Expose
    private String accessories;
    @SerializedName("registration_year")
    @Expose
    private String registrationYear;
    @SerializedName("assembled")
    @Expose
    private String assembled;
    @SerializedName("transmission")
    @Expose
    private String transmission;
    @SerializedName("market_value")
    @Expose
    private String marketValue;
    @SerializedName("missing_terms")
    @Expose
    private String missingTerms;
    @SerializedName("insured_address")
    @Expose
    private String insuredAddress;

    @SerializedName("insurance_company")
    @Expose
    private String insuranceCompany;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("contact_name")
    @Expose
    private String contactName;
    @SerializedName("license_number")
    @Expose
    private String licenseNumber;
    @SerializedName("policy_number")
    @Expose
    private String policyNumber;
    @SerializedName("sum_insured")
    @Expose
    private String sumInsured;
    @SerializedName("suveyor_name")
    @Expose
    private String suveyorName;
    @SerializedName("policy_from_date")
    @Expose
    private String policyFromDate;

    @SerializedName("policy_to_date")
    @Expose
    private String policyToDate;
    @SerializedName("survey_date")
    @Expose
    private String surveyDate;
    @SerializedName("report_date")
    @Expose
    private String reportDate;
    @SerializedName("issue_date")
    @Expose
    private String issueDate;
    @SerializedName("expiry_date")
    @Expose
    private String expiryDate;
    @SerializedName("date_of_loss")
    @Expose
    private String dateOfLoss;
    @SerializedName("date_of_intimation")
    @Expose
    private String dateOfIntimation;
    @SerializedName("registration_date")
    @Expose
    private String registrationDate;

    public String getRegistrationYear() {
        return registrationYear;
    }

    public void setRegistrationYear(String registrationYear) {
        this.registrationYear = registrationYear;
    }

    public String getAssembled() {
        return assembled;
    }

    public void setAssembled(String assembled) {
        this.assembled = assembled;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getMarketValue() {
        return marketValue;
    }

    public void setMarketValue(String marketValue) {
        this.marketValue = marketValue;
    }

    public String getMissingTerms() {
        return missingTerms;
    }

    public void setMissingTerms(String missingTerms) {
        this.missingTerms = missingTerms;
    }

    public String getInsuredAddress() {
        return insuredAddress;
    }

    public void setInsuredAddress(String insuredAddress) {
        this.insuredAddress = insuredAddress;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getRepairer() {
        return repairer;
    }

    public void setRepairer(String repairer) {
        this.repairer = repairer;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getChasisNo() {
        return chasisNo;
    }

    public void setChasisNo(String chasisNo) {
        this.chasisNo = chasisNo;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getLossNo() {
        return lossNo;
    }

    public void setLossNo(String lossNo) {
        this.lossNo = lossNo;
    }

    public String getSurveyDetails() {
        return surveyDetails;
    }

    public void setSurveyDetails(String surveyDetails) {
        this.surveyDetails = surveyDetails;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getBodyObservation() {
        return bodyObservation;
    }

    public void setBodyObservation(String bodyObservation) {
        this.bodyObservation = bodyObservation;
    }

    public String getAccessories() {
        return accessories;
    }

    public void setAccessories(String accessories) {
        this.accessories = accessories;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(String sumInsured) {
        this.sumInsured = sumInsured;
    }

    public String getSuveyorName() {
        return suveyorName;
    }

    public void setSuveyorName(String suveyorName) {
        this.suveyorName = suveyorName;
    }

    public String getPolicyFromDate() {
        return policyFromDate;
    }

    public void setPolicyFromDate(String policyFromDate) {
        this.policyFromDate = policyFromDate;
    }

    public String getPolicyToDate() {
        return policyToDate;
    }

    public void setPolicyToDate(String policyToDate) {
        this.policyToDate = policyToDate;
    }

    public String getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getDateOfLoss() {
        return dateOfLoss;
    }

    public void setDateOfLoss(String dateOfLoss) {
        this.dateOfLoss = dateOfLoss;
    }

    public String getDateOfIntimation() {
        return dateOfIntimation;
    }

    public void setDateOfIntimation(String dateOfIntimation) {
        this.dateOfIntimation = dateOfIntimation;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }
}










