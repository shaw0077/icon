package com.urwasoft.icon.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shahrukh Malik on 9/30/2016.
 */
public class GCMTokenRequest {
    @SerializedName("gcm_token")
    @Expose
    private String gcmToken;

    /**
     *
     * @return
     * The gcmToken
     */
    public String getGcmToken() {
        return gcmToken;
    }

    /**
     *
     * @param gcmToken
     * The gcm_token
     */
    public void setGcmToken(String gcmToken) {
        this.gcmToken = gcmToken;
    }
}
