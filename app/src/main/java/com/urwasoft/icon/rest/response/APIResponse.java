package com.urwasoft.icon.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shahrukh Malik on 2/10/2016.
 */
public class APIResponse<T> {
    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("response")
    @Expose
    private T response;

    @SerializedName("error")
    @Expose
    private ErrorResponse errorResponse;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }

    public void setError(ErrorResponse errorResponse) {
        this.errorResponse = errorResponse;
    }
}
