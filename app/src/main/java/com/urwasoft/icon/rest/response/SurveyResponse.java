package com.urwasoft.icon.rest.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shahrukh on 12/15/2016.
 */

public class SurveyResponse implements Parcelable{
    @SerializedName("idx")
    @Expose
    private Integer idx;
    @SerializedName("ourRef")
    @Expose
    private String ourRef;
    @SerializedName("companyIdx")
    @Expose
    private Integer companyIdx;
    @SerializedName("addedOn")
    @Expose
    private String addedOn;
    @SerializedName("addedby")
    @Expose
    private Integer addedby;
    @SerializedName("policyNumber")
    @Expose
    private String policyNumber;
    @SerializedName("reportdate")
    @Expose
    private String reportdate;
    @SerializedName("registrationNumber")
    @Expose
    private String registrationNumber;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("enginenumber")
    @Expose
    private String enginenumber;
    @SerializedName("chasisnumber")
    @Expose
    private String chasisnumber;
    @SerializedName("driverName")
    @Expose
    private String driverName;
    @SerializedName("issueDate")
    @Expose
    private String issueDate;
    @SerializedName("licensenumber")
    @Expose
    private String licensenumber;
    @SerializedName("expiry")
    @Expose
    private String expiry;
    @SerializedName("dateofLoss")
    @Expose
    private String dateofLoss;
    @SerializedName("dateofintimation")
    @Expose
    private String dateofintimation;
    @SerializedName("surveyDate")
    @Expose
    private String surveyDate;
    @SerializedName("lossNumber")
    @Expose
    private String lossNumber;
    @SerializedName("policyPeriodFrom")
    @Expose
    private String policyPeriodFrom;
    @SerializedName("policyPeriodTo")
    @Expose
    private String policyPeriodTo;
    @SerializedName("sumInsured")
    @Expose
    private String sumInsured;
    @SerializedName("registrationDate")
    @Expose
    private String registrationDate;
    @SerializedName("assignedTo")
    @Expose
    private Integer assignedTo;
    @SerializedName("surveyorName")
    @Expose
    private String surveyorName;
    @SerializedName("insuranceCompanyName")
    @Expose
    private String insuranceCompanyName;
    @SerializedName("repairer")
    @Expose
    private String repairer;
    @SerializedName("contactForSurvey")
    @Expose
    private String contactForSurvey;
    @SerializedName("total_cost")
    @Expose
    private Double totalCost;
    @SerializedName("contactname")
    @Expose
    private String contactName;
    @SerializedName("contactnumber")
    @Expose
    private String contactNumber;
    @SerializedName("registration_year")
    @Expose
    private String registrationYear;
    @SerializedName("assembled")
    @Expose
    private String assembled;
    @SerializedName("transmission")
    @Expose
    private String transmission;
    @SerializedName("market_value")
    @Expose
    private String marketValue;
    @SerializedName("missing_terms")
    @Expose
    private String missingTerms;
    @SerializedName("insured_address")
    @Expose
    private String insuredAddress;
    @SerializedName("body_observation")
    @Expose
    private String bodyObservation;
    @SerializedName("accessories")
    @Expose
    private String accessories;

    protected SurveyResponse(Parcel in) {
        if (in.readByte() == 0) {
            idx = null;
        } else {
            idx = in.readInt();
        }
        ourRef = in.readString();
        if (in.readByte() == 0) {
            companyIdx = null;
        } else {
            companyIdx = in.readInt();
        }
        addedOn = in.readString();
        if (in.readByte() == 0) {
            addedby = null;
        } else {
            addedby = in.readInt();
        }
        policyNumber = in.readString();
        reportdate = in.readString();
        registrationNumber = in.readString();
        make = in.readString();
        model = in.readString();
        enginenumber = in.readString();
        chasisnumber = in.readString();
        driverName = in.readString();
        issueDate = in.readString();
        licensenumber = in.readString();
        expiry = in.readString();
        dateofLoss = in.readString();
        dateofintimation = in.readString();
        surveyDate = in.readString();
        lossNumber = in.readString();
        policyPeriodFrom = in.readString();
        policyPeriodTo = in.readString();
        sumInsured = in.readString();
        registrationDate = in.readString();
        if (in.readByte() == 0) {
            assignedTo = null;
        } else {
            assignedTo = in.readInt();
        }
        surveyorName = in.readString();
        insuranceCompanyName = in.readString();
        repairer = in.readString();
        contactForSurvey = in.readString();
        if (in.readByte() == 0) {
            totalCost = null;
        } else {
            totalCost = in.readDouble();
        }
        contactName = in.readString();
        contactNumber = in.readString();
        registrationYear = in.readString();
        assembled = in.readString();
        transmission = in.readString();
        marketValue = in.readString();
        missingTerms = in.readString();
        insuredAddress = in.readString();
        bodyObservation = in.readString();
        accessories = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (idx == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(idx);
        }
        dest.writeString(ourRef);
        if (companyIdx == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(companyIdx);
        }
        dest.writeString(addedOn);
        if (addedby == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(addedby);
        }
        dest.writeString(policyNumber);
        dest.writeString(reportdate);
        dest.writeString(registrationNumber);
        dest.writeString(make);
        dest.writeString(model);
        dest.writeString(enginenumber);
        dest.writeString(chasisnumber);
        dest.writeString(driverName);
        dest.writeString(issueDate);
        dest.writeString(licensenumber);
        dest.writeString(expiry);
        dest.writeString(dateofLoss);
        dest.writeString(dateofintimation);
        dest.writeString(surveyDate);
        dest.writeString(lossNumber);
        dest.writeString(policyPeriodFrom);
        dest.writeString(policyPeriodTo);
        dest.writeString(sumInsured);
        dest.writeString(registrationDate);
        if (assignedTo == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(assignedTo);
        }
        dest.writeString(surveyorName);
        dest.writeString(insuranceCompanyName);
        dest.writeString(repairer);
        dest.writeString(contactForSurvey);
        if (totalCost == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(totalCost);
        }
        dest.writeString(contactName);
        dest.writeString(contactNumber);
        dest.writeString(registrationYear);
        dest.writeString(assembled);
        dest.writeString(transmission);
        dest.writeString(marketValue);
        dest.writeString(missingTerms);
        dest.writeString(insuredAddress);
        dest.writeString(bodyObservation);
        dest.writeString(accessories);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SurveyResponse> CREATOR = new Creator<SurveyResponse>() {
        @Override
        public SurveyResponse createFromParcel(Parcel in) {
            return new SurveyResponse(in);
        }

        @Override
        public SurveyResponse[] newArray(int size) {
            return new SurveyResponse[size];
        }
    };

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getOurRef() {
        return ourRef;
    }

    public void setOurRef(String ourRef) {
        this.ourRef = ourRef;
    }

    public Integer getCompanyIdx() {
        return companyIdx;
    }

    public void setCompanyIdx(Integer companyIdx) {
        this.companyIdx = companyIdx;
    }

    public String getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(String addedOn) {
        this.addedOn = addedOn;
    }

    public Integer getAddedby() {
        return addedby;
    }

    public void setAddedby(Integer addedby) {
        this.addedby = addedby;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getReportdate() {
        return reportdate;
    }

    public void setReportdate(String reportdate) {
        this.reportdate = reportdate;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEnginenumber() {
        return enginenumber;
    }

    public void setEnginenumber(String enginenumber) {
        this.enginenumber = enginenumber;
    }

    public String getChasisnumber() {
        return chasisnumber;
    }

    public void setChasisnumber(String chasisnumber) {
        this.chasisnumber = chasisnumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getLicensenumber() {
        return licensenumber;
    }

    public void setLicensenumber(String licensenumber) {
        this.licensenumber = licensenumber;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getDateofLoss() {
        return dateofLoss;
    }

    public void setDateofLoss(String dateofLoss) {
        this.dateofLoss = dateofLoss;
    }

    public String getDateofintimation() {
        return dateofintimation;
    }

    public void setDateofintimation(String dateofintimation) {
        this.dateofintimation = dateofintimation;
    }

    public String getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    public String getLossNumber() {
        return lossNumber;
    }

    public void setLossNumber(String lossNumber) {
        this.lossNumber = lossNumber;
    }

    public String getPolicyPeriodFrom() {
        return policyPeriodFrom;
    }

    public void setPolicyPeriodFrom(String policyPeriodFrom) {
        this.policyPeriodFrom = policyPeriodFrom;
    }

    public String getPolicyPeriodTo() {
        return policyPeriodTo;
    }

    public void setPolicyPeriodTo(String policyPeriodTo) {
        this.policyPeriodTo = policyPeriodTo;
    }

    public String getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(String sumInsured) {
        this.sumInsured = sumInsured;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(Integer assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getSurveyorName() {
        return surveyorName;
    }

    public void setSurveyorName(String surveyorName) {
        this.surveyorName = surveyorName;
    }

    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    public void setInsuranceCompanyName(String insuranceCompanyName) {
        this.insuranceCompanyName = insuranceCompanyName;
    }

    public String getRepairer() {
        return repairer;
    }

    public void setRepairer(String repairer) {
        this.repairer = repairer;
    }

    public String getContactForSurvey() {
        return contactForSurvey;
    }

    public void setContactForSurvey(String contactForSurvey) {
        this.contactForSurvey = contactForSurvey;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getRegistrationYear() {
        return registrationYear;
    }

    public void setRegistrationYear(String registrationYear) {
        this.registrationYear = registrationYear;
    }

    public String getAssembled() {
        return assembled;
    }

    public void setAssembled(String assembled) {
        this.assembled = assembled;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getMarketValue() {
        return marketValue;
    }

    public void setMarketValue(String marketValue) {
        this.marketValue = marketValue;
    }

    public String getMissingTerms() {
        return missingTerms;
    }

    public void setMissingTerms(String missingTerms) {
        this.missingTerms = missingTerms;
    }

    public String getInsuredAddress() {
        return insuredAddress;
    }

    public void setInsuredAddress(String insuredAddress) {
        this.insuredAddress = insuredAddress;
    }

    public String getBodyObservation() {
        return bodyObservation;
    }

    public void setBodyObservation(String bodyObservation) {
        this.bodyObservation = bodyObservation;
    }

    public String getAccessories() {
        return accessories;
    }

    public void setAccessories(String accessories) {
        this.accessories = accessories;
    }
}
