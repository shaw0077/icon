package com.urwasoft.icon.rest;


import com.urwasoft.icon.rest.response.APIListResponse;

import retrofit2.Response;

/**
 * Created by Shahrukh Malik on 2/12/2016.
 */
public interface INetworkList<T> {
    void onResponseList(Response<APIListResponse<T>> response);
    void onErrorList(Response<APIListResponse<T>> response);
    void onNullListResponse();
    void onFailureList(Throwable t);
}
