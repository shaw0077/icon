package com.urwasoft.icon.controller;


import com.urwasoft.icon.rest.INetwork;

/**
 * Created by Shahrukh Malik on 9/7/2016.
 */
public class LoginController<T> {
    private INetwork iNetwork;

    public LoginController(INetwork iNetwork){
        this.iNetwork = iNetwork;
    }

    /*public void callSignUp(SignUpRequest signUpRequest, String locale){
        Call<APIResponse<UserResponse>> call = RestClient.get().getApiService().signUp(signUpRequest,locale);
        call.enqueue(new Callback<APIResponse<UserResponse>>() {
            @Override
            public void onResponse(Call<APIResponse<UserResponse>> call, Response<APIResponse<UserResponse>> response) {
                if(response == null){
                    iNetwork.onNullResponse();
                }
                if(response.isSuccessful()){
                    if(response.body() == null){
                        iNetwork.onNullResponse();
                    }
                    if(response.body().getStatus()){
                        iNetwork.onResponse(response);
                    }
                    else {
                        iNetwork.onError(response);
                    }
                }
                else {
                    iNetwork.onError(response);
                }
            }

            @Override
            public void onFailure(Call<APIResponse<UserResponse>> call, Throwable t) {
                iNetwork.onFailure(t);
            }
        });
    }

    public void callSignIn(String username,String password,String token1,String token2){
        Call<UserResponse> call = RestClient.get().getApiService().signIn(username,password,token1,token2);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response == null){
                    iNetwork.onNullResponse();
                }
                if(response.isSuccessful()){
                    if(response.body() == null){
                        iNetwork.onNullResponse();
                    }
                    if(response.body().getStatus()){
                        iNetwork.onResponse(response);
                    }
                    else {
                        iNetwork.onError(response);
                    }
                }
                else {
                    iNetwork.onError(response);
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                iNetwork.onFailure(t);
            }
        });
    }

    public void callSignInUpWithFacebook(FacebookSignInUpRequest facebookSignInUpRequest, String locale){
        Call<APIResponse<UserResponse>> call = RestClient.get().getApiService().signInUpWithFacebook(facebookSignInUpRequest,locale);
        call.enqueue(new Callback<APIResponse<UserResponse>>() {
            @Override
            public void onResponse(Call<APIResponse<UserResponse>> call, Response<APIResponse<UserResponse>> response) {
                if(response == null){
                    iNetwork.onNullResponse();
                }
                if(response.isSuccessful()){
                    if(response.body() == null){
                        iNetwork.onNullResponse();
                    }
                    if(response.body().getStatus()){
                        iNetwork.onResponse(response);
                    }
                    else {
                        iNetwork.onError(response);
                    }
                }
                else {
                    iNetwork.onError(response);
                }
            }

            @Override
            public void onFailure(Call<APIResponse<UserResponse>> call, Throwable t) {
                iNetwork.onFailure(t);
            }
        });
    }

    public void callForgotPassword(ForgotPasswordRequest forgotPasswordRequest, String locale){
        Call<APIResponse<UserResponse>> call = RestClient.get().getApiService().forgotPassword(forgotPasswordRequest,locale);
        call.enqueue(new Callback<APIResponse<UserResponse>>() {
            @Override
            public void onResponse(Call<APIResponse<UserResponse>> call, Response<APIResponse<UserResponse>> response) {
                if(response == null){
                    iNetwork.onNullResponse();
                }
                if(response.isSuccessful()){
                    if(response.body() == null){
                        iNetwork.onNullResponse();
                    }
                    if(response.body().getStatus()){
                        iNetwork.onResponse(response);
                    }
                    else {
                        iNetwork.onError(response);
                    }
                }
                else {
                    iNetwork.onError(response);
                }
            }

            @Override
            public void onFailure(Call<APIResponse<UserResponse>> call, Throwable t) {
                iNetwork.onFailure(t);
            }
        });
    }

    public APIResponse<T> parseError(Response<?> response) {
        Converter<ResponseBody, APIResponse<T>> converter =
                RestClient.get().getRetrofit()
                        .responseBodyConverter(APIResponse.class, new Annotation[0]);
        APIResponse<T> error;
        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIResponse<T>();
        }

        return error;
    }*/
}
