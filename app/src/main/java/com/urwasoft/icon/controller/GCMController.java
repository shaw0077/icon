package com.urwasoft.icon.controller;


import com.urwasoft.icon.rest.INetwork;
import com.urwasoft.icon.rest.RestClient;
import com.urwasoft.icon.rest.request.AccessToken;
import com.urwasoft.icon.rest.request.GCMTokenRequest;
import com.urwasoft.icon.rest.response.APIResponse;
import com.urwasoft.icon.rest.response.UserResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by Shahrukh Malik on 3/25/2016.
 */
public class GCMController<T> {
    private INetwork iNetwork;
    private AccessToken accessToken;

    public GCMController(INetwork iNetwork,AccessToken accessToken){
        this.iNetwork = iNetwork;
        this.accessToken = accessToken;
    }

    public void callUpdateGCMToken(GCMTokenRequest gcmTokenRequest, String locale){
        Call<APIResponse<UserResponse>> call = RestClient.get(accessToken).getApiService().updateGCMToken(gcmTokenRequest,locale);
        call.enqueue(new Callback<APIResponse<UserResponse>>() {
            @Override
            public void onResponse(Call<APIResponse<UserResponse>> call, Response<APIResponse<UserResponse>> response) {
                if(response == null){
                    iNetwork.onNullResponse();
                }
                if(response.isSuccessful()){
                    if(response.body() == null){
                        iNetwork.onNullResponse();
                    }
                    if(response.body().getStatus()){
                        iNetwork.onResponse(response);
                    }
                    else {
                        iNetwork.onError(response);
                    }
                }
                else {
                    iNetwork.onError(response);
                }
            }

            @Override
            public void onFailure(Call<APIResponse<UserResponse>> call, Throwable t) {
                iNetwork.onFailure(t);
            }
        });
    }

    public APIResponse<T> parseError(Response<?> response) {
        Converter<ResponseBody, APIResponse<T>> converter =
                RestClient.get().getRetrofit()
                        .responseBodyConverter(APIResponse.class, new Annotation[0]);
        APIResponse<T> error;
        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIResponse<T>();
        }

        return error;
    }
}
