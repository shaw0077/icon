package com.urwasoft.icon.controller;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.urwasoft.icon.R;


/**
 * Created by Shahrukh Malik on 4/5/2016.
 */
public class ImageLoadingController { // this controller is using Glide, an image loading/caching library recommended by Google for Android
    private Context context;

    public ImageLoadingController(Context context)
    {
        this.context = context;
    }

    public void loadImage(ImageView img, String url)
    {
        Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .placeholder(R.drawable.logo)
                .animate(android.R.anim.fade_in)
                .into(img);
    }

    public void loadImagePrescription(ImageView img, String url)
    {
        Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .placeholder(R.mipmap.ic_launcher)
                .animate(android.R.anim.fade_in)
                .into(img);
    }


    /*public void loadImageAndBlurItVendorCoverImage(final ImageView img, String url)
    {
        Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .placeholder(R.drawable.place_holder_service_detail)
                .animate(android.R.anim.fade_in)
                .into(new GlideDrawableImageViewTarget(img) {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                        super.onResourceReady(resource, animation);
                        Blurry.with(context)
                                .radius(6)
                                .sampling(6)
                                .capture(img)
                                .into(img);
                    }
                });
    }*/

}
