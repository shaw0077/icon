package com.urwasoft.icon.util;


import com.crashlytics.android.Crashlytics;
import com.urwasoft.icon.BuildConfig;

import net.gotev.uploadservice.UploadService;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Shahrukh Malik on 4/8/2016.
 */
public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        /*CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/roboto_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );*/

        // setup the broadcast action namespace string which will
        // be used to notify upload status.
        // Gradle automatically generates proper variable as below.
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
        // Or, you can define it manually.
        UploadService.NAMESPACE = "com.urwasoft.icon";
    }
}








