package com.urwasoft.icon.util;

/**
 * Created by Shahrukh Malik on 2/24/2016.
 */
public class AppConstants {
    public static final String BASE_URL_DEV = "http://192.168.9.8";
    public static final String BASE_URL_PRODUCTION = "http://iconapi.urwasoft.com";
    public static final String BASE_URL_PRODUCTION_2 = "http://54.191.182.125";
    public static final String GRANT_TYPE = "password";
    public static final String APIARY_ANDROID_CLIENT_ID = "celebrity-app-android";
    public static final String APIARY_ANDROID_KEY = "Y2VsZWJyaXR5LWFwcC1hbmRyb2lkOjE4OTExMWFiLWVmMWItNGNkOS05MGRiLTliNjk1MDFjZGU2Nw==";
    public static final String BEARER = "Bearer";
    public static final int STATUS_SUCCESS = 200;
    public static final int STATUS_UNAUTHORIZED = 401;
    //public static final int ANIMATION_DURATION_VENDOR_HOME = 200;
    public static final int ANIMATION_DURATION_SLIDER_HOME = 150;
    public static final int ANIMATION_DURATION_SIGN_IN_UP = 200;
    public static final String STATUS_PENDING = "PENDING";
    public static final String STATUS_ACCEPTED = "ACCEPTED";
    public static final String STATUS_REJECTED = "REJECTED";
    // delay to launch nav drawer item, to allow close animation to play
    public static final int NAVDRAWER_LAUNCH_DELAY = 250;
    public static final int MAIN_CONTENT_FADEOUT_DURATION = 150;
    public static final int MAIN_CONTENT_FADEIN_DURATION = 250;
    public static final String DATE_FORMAT_ONE = "yyyy-MM-dd HH:mm:ss z";
    public static final String DATE_FORMAT_TWO = "dd MMM yyyy";
    public static final String DATE_FORMAT_THREE = "MMM dd";
    public static final String DATE_FORMAT_FOUR = "MMM dd, yyyy";
    public static final String DATE_FORMAT_FIVE = "yyyy:MM:dd";
    public static final String DATE_FORMAT_SIX = "MMM dd, yyyy";
    public static final String DATE_FORMAT_SEVEN = "yyyy-MM-dd'T'HH:mm:ss";

    public static final String TOKEN_ONE = "u";
    public static final String TOKEN_TWO = "u";
}














