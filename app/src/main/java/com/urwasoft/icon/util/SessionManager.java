package com.urwasoft.icon.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.urwasoft.icon.activity.SignInActivity;

import static com.facebook.internal.FacebookRequestErrorClassification.KEY_NAME;


/**
 * Created by Shahrukh Malik on 2/29/2016.
 */
public class SessionManager {
    private static SessionManager sessionManager;
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "com.mobiwhiz.izhalha.MECHANIC";
    private static final String IS_LOGIN = "isLoggedIn";

    private static final String KEY_USER_ID = "userId";
    private static final String KEY_USERNAME = "user_name";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_EMP_ID = "emp_id";
    private static final String KEY_FULL_NAME = "full_name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_DEPT_NAME = "dept_name";
    private static final String KEY_USER_RESPONSE = "user_response";
    private static final String KEY_FCM_TOKEN = "fcm_token";
    private static final String KEY_IMAGE_FILE_URL = "image_file_url";
    private static final String KEY_ACCESS_TOKEN = "access_token";

    private static final String KEY_IS_ARABIC = "isArabic";
    private static final String KEY_IS_NOTIFICATION_ENABLED = "isNotificationEnabled";
    public static final String LOCALE = "en";



    // Constructor
    private SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static SessionManager get(Context context)
    {
        if(sessionManager == null){
            sessionManager = new SessionManager(context);
        }
        return sessionManager;
    }

    /**
     * Create fragment_login session
     * */
    public void createLoginSession(int userId, String username,String password,int empId, String fullname, String email,String deptName,String jsonUserResponse){
        editor.putBoolean(IS_LOGIN, true);
        editor.putInt(KEY_USER_ID, userId);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_PASSWORD, password);
        editor.putInt(KEY_EMP_ID, empId);
        editor.putString(KEY_FULL_NAME, fullname);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_DEPT_NAME, deptName);
        editor.putString(KEY_USER_RESPONSE, jsonUserResponse);
        editor.commit();
    }

    /**
     * Check fragment_login method wil check user fragment_login status
     * If false it will redirect user to fragment_login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check fragment_login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            /*Intent i = new Intent(_context, SliderActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            _context.startActivity(i);*/
        }
    }

    public int getUserId()
    {
        return pref.getInt(KEY_USER_ID, 0);
    }
    public String getName()
    {
        String name = pref.getString(KEY_NAME, null);
        if(name != null){
            return name;
        }else {
            return "";
        }
    }
    public String getEmail()
    {
        String email = pref.getString(KEY_EMAIL, null);
        if(email != null){
            return email;
        }else {
            return "";
        }
    }

    public String getAccessToken()
    {
        String accessToken = pref.getString(KEY_ACCESS_TOKEN, null);
        if(accessToken != null){
            return accessToken;
        }else {
            return "";
        }
    }

    public void setImageFileURL(String fileURL)
    {
        editor.putString(KEY_IMAGE_FILE_URL,fileURL);
        editor.commit();
    }

    public String getImageFileURL()
    {
        String imageFileUrl = pref.getString(KEY_IMAGE_FILE_URL, null);
        if(imageFileUrl != null){
            return imageFileUrl;
        }else {
            return "";
        }
    }

    public String getUserResponse()
    {
        String userResponse = pref.getString(KEY_USER_RESPONSE, null);
        if(userResponse != null){
            return userResponse;
        }else {
            return "";
        }
    }

    public void setUserResponse(String jsonUserResponse)
    {
        editor.putString(KEY_USER_RESPONSE,jsonUserResponse);
        editor.commit();
    }

    public void setIsArabic(boolean isArabic)
    {
        editor.putBoolean(KEY_IS_ARABIC, isArabic);
        editor.commit();
    }

    public boolean isArabic()
    {
        return pref.getBoolean(KEY_IS_ARABIC, false);
    }

    public void setIsNotificationEnabled(boolean isNotificationEnabled)
    {
        editor.putBoolean(KEY_IS_NOTIFICATION_ENABLED, isNotificationEnabled);
        editor.commit();
    }

    public boolean isNotificationEnabled()
    {
        return pref.getBoolean(KEY_IS_NOTIFICATION_ENABLED, true);
    }

    public void setFCMToken(String token)
    {
        editor.putString(KEY_FCM_TOKEN, token);
        editor.commit();
    }

    public String getFCMToken()
    {
        return pref.getString(KEY_FCM_TOKEN, "");
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        editor.remove(IS_LOGIN);
        editor.remove(KEY_USER_ID);
        editor.remove(KEY_USERNAME);
        editor.remove(KEY_PASSWORD);
        editor.remove(KEY_EMP_ID);
        editor.remove(KEY_FULL_NAME);
        editor.remove(KEY_EMAIL);
        editor.remove(KEY_DEPT_NAME);
        editor.remove(KEY_USER_RESPONSE);
        editor.apply();
        editor.commit();

        Intent intent = new Intent(_context, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        _context.startActivity(intent);

        /*Profile profile = Profile.getCurrentProfile().getCurrentProfile();
        if (profile != null) {
            // user has logged in
            LoginManager.getInstance().logOut();
        } else {
            // user has not logged in
        }

        RestClient.clearInstance();
        Intent intent = new Intent(_context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        _context.startActivity(intent);*/
    }

    /**
     * Quick check for fragment_login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }


}



















