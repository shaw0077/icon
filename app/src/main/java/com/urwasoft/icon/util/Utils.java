package com.urwasoft.icon.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.urwasoft.icon.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Shahrukh Malik on 2/24/2016.
 */
public class Utils {
    private Context context;

    public Utils(Context context){
        this.context = context;
    }

    public int getScreenWidth(Activity activity)
    {
        if(activity != null) {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            return metrics.widthPixels;
        }
        else {
            return 0;
        }
    }

    public int getScreenHeight(Activity activity)
    {
        if(activity != null) {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            return metrics.heightPixels;
        }
        else {
            return 0;
        }
    }

    public void updateEditText(EditText edt, String text)
    {
        edt.setText(text);
        edt.setSelection(edt.getText().length());
    }

    public boolean isInternetAvailable() {
        /*if(context != null) {
            try {
                ConnectivityManager cm =
                        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                return isConnected;
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }else {
            return false;
        }*/
        return isInternetAvailableMoreAccurate();
    }

    public boolean isInternetAvailableMoreAccurate() {
        if (context != null) {
            try {
                boolean haveConnectedWifi = false;
                boolean haveConnectedMobile = false;
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo[] netInfo = cm.getAllNetworkInfo();
                for (NetworkInfo ni : netInfo) {
                    if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                        if (ni.isConnected()) {
                            if (!(ni.getExtraInfo().toLowerCase().contains("theta")))
                                haveConnectedWifi = true;
                        }
                    if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                        if (ni.isConnected())
                            haveConnectedMobile = true;
                }
                return haveConnectedWifi || haveConnectedMobile;
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    public String removeLeadingAndTrailingSpaces(String str){
        return str.replaceAll("^\\s+|\\s+$", "");
    }

    public boolean isEditTextNullOrEmpty(EditText edt){
        if(edt.getText() != null){
            if(edt.getText().toString().trim().isEmpty()){
                return true;
            }
            else {
                String trimmed = removeLeadingAndTrailingSpaces(edt.getText().toString());
                edt.setText(trimmed);
				edt.setSelection(edt.getText().length());
                return false;
            }
        }
        else {
            return true;
        }
    }

    public boolean isEditTextNullOrEmpty(ActionEditText edt){
        if(edt.getText() != null){
            if(edt.getText().toString().trim().isEmpty()){
                return true;
            }
            else {
                String trimmed = removeLeadingAndTrailingSpaces(edt.getText().toString());
                edt.setText(trimmed);
                edt.setSelection(edt.getText().length());
                return false;
            }
        }
        else {
            return true;
        }
    }

    public boolean isValidEmail(String email){
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public String getStringFromResourceId(int stringResourceId){
        if(context != null) {
            return context.getResources().getString(stringResourceId);
        }
        else {
            return "";
        }
    }

    /*public long getGMTTimestampByDate(CalendarDay date){
        long timestamp = 1457031600;
        try {
            DateFormat dateFormatGmt = new SimpleDateFormat("dd MM yyyy");
            dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
            String dateString = String.valueOf(date.getDay()) + " " + String.valueOf(date.getMonth()+1) + " " + String.valueOf(date.getYear());
            Date date1 = dateFormatGmt.parse(dateString);
            long unixTime = (long)date1.getTime()/1000;
            return unixTime;
        }
        catch (ParseException ex){
            ex.printStackTrace();
        }
        return timestamp;
    }*/

    public String getFormattedDate(String dateString,String inputFormat,String outputFormat) throws ParseException {
        try {
            DateFormat dateFormatGmt1 = new SimpleDateFormat(inputFormat);
            Date date = dateFormatGmt1.parse(dateString);

            SimpleDateFormat sdf = new SimpleDateFormat(outputFormat);
            return sdf.format(date);
        }catch (Exception ex){
            ex.printStackTrace();
            return "-";
        }
    }

    public String getFormattedDateByDotNetDateFormat(String dateStringOriginal) throws ParseException {
        DateFormat dateFormatGmt1 = new SimpleDateFormat(AppConstants.DATE_FORMAT_SEVEN);
        Date date = dateFormatGmt1.parse(dateStringOriginal);

        SimpleDateFormat sdf = new SimpleDateFormat(AppConstants.DATE_FORMAT_FOUR); // the format of your date
        return sdf.format(date);
    }

    public long getGMTTimestampByDate(String dateStringOriginal) throws ParseException {
        DateFormat dateFormatGmt1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date = dateFormatGmt1.parse(dateStringOriginal);
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        calendar.setTime(date);
        String dateString = String.valueOf(calendar.get(Calendar.YEAR)) + "-"
                + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-"
                + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))  + " "
                + String.valueOf(calendar.get(Calendar.HOUR_OF_DAY))   + ":"
                + String.valueOf(calendar.get(Calendar.MINUTE))  + ":"
                + String.valueOf(calendar.get(Calendar.SECOND));

        DateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date1 = dateFormatGmt.parse(dateString);
        long unixTime = (long)date1.getTime()/1000;
        return unixTime;
    }

    public String getDateInStringByTimestamp(long timestamp, String format){
        Date date = new Date(timestamp*1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat(format); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT")); // give a timezone reference for formating (see comment at the bottom
        return sdf.format(date);
    }

    public long getGMTTimestampByJavaDateString(String dateStringOriginal) throws ParseException {
        DateFormat dateFormatGmt1 = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
        Date date = dateFormatGmt1.parse(dateStringOriginal);
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        calendar.setTime(date);
        String dateString = String.valueOf(calendar.get(Calendar.YEAR)) + "-"
                + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-"
                + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))  + " "
                + String.valueOf(calendar.get(Calendar.HOUR_OF_DAY))   + ":"
                + String.valueOf(calendar.get(Calendar.MINUTE))  + ":"
                + String.valueOf(calendar.get(Calendar.SECOND));

        DateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date1 = dateFormatGmt.parse(dateString);
        long unixTime = (long)date1.getTime()/1000;
        return unixTime;
    }

    public String getTimeAMPMByString(String timeInColonsSeperated /* 01:00:00 */){
        try {
            String[] splits = timeInColonsSeperated.split(":");
            if(splits[0].equals("12")){
                return "12" + ":" + splits[1] +  " " + getStringFromResourceId(R.string.pm);
            }
            if(splits[0].equals("00")){
                return "12" + ":" + splits[1] +  " " + getStringFromResourceId(R.string.am);
            }
            if(splits[0].equals("24")){
                return "12" + ":" + splits[1] +  " " + getStringFromResourceId(R.string.am);
            }
            Date date1;
            String time = "";
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
            date1 = sdf.parse(timeInColonsSeperated);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date1);
            int ampm = calendar.get(Calendar.AM_PM);
            int hour = calendar.get(Calendar.HOUR);
            int minute = calendar.get(Calendar.MINUTE);

            String hourString = "";
            String minuteString = "";
            if(hour<10){
                hourString = "0" + String.valueOf(hour);
            }else{
                hourString = String.valueOf(hour);
            }
            if(minute<10){
                minuteString = "0" + String.valueOf(minute);
            }else{
                minuteString = String.valueOf(minute);
            }
            time = hourString +":"+minuteString;
            if(ampm == 0){
                time += " " + getStringFromResourceId(R.string.am);
            }else {
                time += " " + getStringFromResourceId(R.string.pm);
            }

            return time;
        }catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
    }

    public String addOneHourToTime(String timeInColonsSeperated){
        try {
            String time = "";
            try {
                String[] splits = timeInColonsSeperated.split(":");
                int hour = Integer.valueOf(splits[0]);
                hour += 1;
                time = String.valueOf(hour) +":"+ splits[1] +":"+ splits[2];
            }catch (Exception ex){
                return "";
            }
            return time;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
    }

    public int getCurrentGMTToAddSubtract(){
        try {
            Date date = new Date();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int rawOffset = calendar.getTimeZone().getRawOffset();

            int hoursToAddMinus = rawOffset / 3600000;

            return hoursToAddMinus;
        }catch (Exception ex)
        {
            ex.printStackTrace();
            return 0;
        }
    }

    public String addGMTToTime(String timeInColonsSeperated /* 01:00:00 */){
        try {
            String[] splits = timeInColonsSeperated.split(":");
            int hour = Integer.parseInt(splits[0]);
            hour += getCurrentGMTToAddSubtract();

            String hourString = "";
            if(hour < 10){
                hourString += "0" + String.valueOf(hour);
            }else {
                hourString += String.valueOf(hour);
            }
            return hourString +":"+ splits[1] +":"+ splits[2];
        }catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
    }

    public String subtractGMTFromTime(String timeInColonsSeperated /* 01:00:00 */){
        try {
            String[] splits = timeInColonsSeperated.split(":");
            int hour = Integer.parseInt(splits[0]);
            hour -= getCurrentGMTToAddSubtract();

            String hourString = "";
            if(hour < 10){
                hourString += "0" + String.valueOf(hour);
            }else {
                hourString += String.valueOf(hour);
            }
            return hourString +":"+ splits[1] +":"+ splits[2];
        }catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
    }

    public void setActivityAppLocale(Context context){
        if(context != null) {
            if (SessionManager.get(context).isArabic()) {
                String languageToLoad = "ar";
                SessionManager.get(context).setIsArabic(true);
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            } else {
                SessionManager.get(context).setIsArabic(false);
                String languageToLoad = "en";
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            }
        }
    }

    public Bitmap saveViewAsImage(View v)
    {
        Bitmap image = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.RGB_565);
        v.draw(new Canvas(image));
        return image;
    }

    public String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

    public boolean onLoadDataInitialize(View view, AVLoadingIndicatorView avLoadingIndicatorView, LinearLayout linearLayout){
        if(context != null) {
            if (!(isInternetAvailable())) {
                view.setVisibility(View.GONE);
                avLoadingIndicatorView.setVisibility(View.GONE);
                linearLayout.setVisibility(View.VISIBLE);
                return false;
            } else {
                view.setVisibility(View.GONE);
                avLoadingIndicatorView.setVisibility(View.VISIBLE);
                linearLayout.setVisibility(View.GONE);
                return true;
            }
        }
        else {
            return false;
        }
    }

    public boolean onLoadDataInitialize(View view, AVLoadingIndicatorView avLoadingIndicatorView, LinearLayout linearLayout, Button btnSale){
        if(context != null) {
            if (!(isInternetAvailable())) {
                view.setVisibility(View.GONE);
                btnSale.setVisibility(View.GONE);
                avLoadingIndicatorView.setVisibility(View.GONE);
                linearLayout.setVisibility(View.VISIBLE);
                return false;
            } else {
                view.setVisibility(View.GONE);
                btnSale.setVisibility(View.GONE);
                avLoadingIndicatorView.setVisibility(View.VISIBLE);
                linearLayout.setVisibility(View.GONE);
                return true;
            }
        }
        else {
            return false;
        }
    }

    public void onEmptyData(View view,AVLoadingIndicatorView avLoadingIndicatorView,LinearLayout linearLayout){
        view.setVisibility(View.GONE);
        avLoadingIndicatorView.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
    }

    public void onDataReceived(View view,AVLoadingIndicatorView avLoadingIndicatorView,LinearLayout linearLayout){
        view.setVisibility(View.VISIBLE);
        avLoadingIndicatorView.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
    }

    public void onDataReceived(View view,AVLoadingIndicatorView avLoadingIndicatorView,LinearLayout linearLayout,Button btnSale){
        btnSale.setVisibility(View.VISIBLE);
        view.setVisibility(View.VISIBLE);
        avLoadingIndicatorView.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
    }

    public void onErrorReceived(View view,AVLoadingIndicatorView avLoadingIndicatorView,LinearLayout linearLayout, String errorMesg){
        view.setVisibility(View.GONE);
        avLoadingIndicatorView.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);
        TextView txtRetry = (TextView) linearLayout.getChildAt(0);
        txtRetry.setText(errorMesg);
    }

    // convert from UTF-8 -> internal Java String format
    public String convertFromUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("ISO-8859-1"), "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

    // convert from internal Java String format -> UTF-8
    public String convertToUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

    public void hideSoftKeyboard(EditText editText){
        if(context != null){
            InputMethodManager imm = (InputMethodManager)
            context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(
                    editText.getWindowToken(), 0);
        }
    }

    public void showSoftKeyboard(EditText editText){
        if(context != null){
            editText.requestFocus();
            InputMethodManager imm = (InputMethodManager)
                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText,
                    InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public void underlineTextview(TextView txt,String text){
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        txt.setText(content);
    }

    /*public static final int[] timeSlotsInWords = {
            R.string.twelve_am_to_one_am,
            R.string.one_am_to_two_am,
            R.string.two_am_to_three_am,
            R.string.three_am_to_four_am,
            R.string.four_am_to_five_am,
            R.string.five_am_to_six_am,
            R.string.six_am_to_seven_am,
            R.string.seven_am_to_eight_am,
            R.string.eight_am_to_nine_am,
            R.string.nine_am_to_ten_am,
            R.string.ten_am_to_eleven_am,
            R.string.eleven_am_to_twelve_pm,
            R.string.twelve_pm_to_one_pm,
            R.string.one_pm_to_two_pm,
            R.string.two_pm_to_three_pm,
            R.string.three_pm_to_four_pm,
            R.string.four_pm_to_five_pm,
            R.string.five_pm_to_six_pm,
            R.string.six_pm_to_seven_pm,
            R.string.seven_pm_to_eight_pm,
            R.string.eight_pm_to_nine_pm,
            R.string.nine_pm_to_ten_pm,
            R.string.ten_pm_to_eleven_pm,
            R.string.eleven_pm_to_twelve_am,
    };

    public static final int[] timeSlotInWords = {
            R.string.twelve_am,
            R.string.one_am,
            R.string.two_am,
            R.string.three_am,
            R.string.four_am,
            R.string.five_am,
            R.string.six_am,
            R.string.seven_am,
            R.string.eight_am,
            R.string.nine_am,
            R.string.ten_am,
            R.string.eleven_am,
            R.string.twelve_pm,
            R.string.one_pm,
            R.string.two_pm,
            R.string.three_pm,
            R.string.four_pm,
            R.string.five_pm,
            R.string.six_pm,
            R.string.seven_pm,
            R.string.eight_pm,
            R.string.nine_pm,
            R.string.ten_pm,
            R.string.eleven_pm,
    };*/

    public String formatDouble(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }

    public String convertNumberStringToArabic(String number){
        String arabicFinal = "";
        for(int i=0;i<number.length();i++){
            int ascii = (int)number.charAt(i);

            if( (ascii >= 48) && (ascii <= 57) ){
                arabicFinal += arabicNumbers[ascii-48];
            }else{
                arabicFinal += number.charAt(i);
            }
        }
        return arabicFinal;
    }

    public static final String[] arabicNumbers = {
            "٠",
            "١",
            "٢",
            "٣",
            "٤",
            "٥",
            "٦",
            "٧",
            "٨",
            "٩"
    };

    public String convertArabicNumberToNumber(String number){
        String arabicFinal = "";
        for(int i=0;i<number.length();i++){
            int ascii = (int)number.charAt(i);

            if( (ascii >= 1632) && (ascii <= 1641) ){
                arabicFinal += numbers[ascii-1632];//number.charAt(ascii - 48);
            }else{
                arabicFinal += number.charAt(i);
            }
        }
        return arabicFinal;
    }

    public static final String[] numbers = {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"
    };
}





































