package com.urwasoft.icon.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.urwasoft.icon.BuildConfig;
import com.urwasoft.icon.R;
import com.urwasoft.icon.controller.ImageLoadingController;
import com.urwasoft.icon.controller.ProgressCustomDialogController;
import com.urwasoft.icon.rest.RestClient;
import com.urwasoft.icon.rest.response.ErrorResponse2;
import com.urwasoft.icon.util.AppConstants;
import com.urwasoft.icon.util.PermissionUtils;
import com.urwasoft.icon.util.SessionManager;
import com.urwasoft.icon.util.SnackUtil;
import com.urwasoft.icon.util.Utils;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import static com.facebook.GraphRequest.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotosFragment extends Fragment implements View.OnClickListener,ImagePickerCallback,UploadStatusDelegate {
    private Button btnCamera,btnGallery,btnDone;
    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private ImageLoadingController imageLoadingController;
    private String pickerPath;
    private SnackUtil snackUtil;
    private String imageUrlMain = "";
    private ProgressCustomDialogController progressCustomDialogControllerPleaseWait;
    private ProgressCustomDialogController progressCustomDialogControllerUploadingImage;
    private RecyclerView recyclerViewImages;
    private ImagesAdapter adapter;
    private Utils utils;
    private ArrayList<String> images;
    private boolean isCameraPressed = true;

    public static final String IMAGES = "com.urwasoft.icon.IMAGES";

    public PhotosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        images = getActivity().getIntent().getStringArrayListExtra(IMAGES);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_photos, container, false);

        initializeViews(v);

        return v;
    }

    private void initializeViews(View v){
        btnCamera = (Button) v.findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(this);
        btnGallery = (Button) v.findViewById(R.id.btnGallery);
        btnGallery.setOnClickListener(this);
        btnDone = (Button) v.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(this);
        recyclerViewImages = (RecyclerView) v.findViewById(R.id.recyclerViewImages);
        recyclerViewImages.setLayoutManager(new GridLayoutManager(getActivity(),3));

        imageLoadingController = new ImageLoadingController(getActivity());
        snackUtil = new SnackUtil(getActivity());
        utils = new Utils(getActivity());
        progressCustomDialogControllerPleaseWait = new ProgressCustomDialogController(getActivity(),R.string.please_wait);
        progressCustomDialogControllerUploadingImage = new ProgressCustomDialogController(getActivity(),R.string.uploading_image);

        if(images != null){
            for(String image : images){
                addImagesToAdapter(image);
            }
        }
    }

    private void addImagesToAdapter(String image){
        if(adapter == null){
            List<String> images = new ArrayList<>();
            images.add(image);
            adapter = new ImagesAdapter(images);
            recyclerViewImages.setAdapter(adapter);
        }else {
            adapter.images.add(image);
            adapter.notifyItemInserted(adapter.images.size() - 1);
        }
    }

    private void addImagesToAdapter(List<String> images){

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnCamera:{
                //takePicture();
                isCameraPressed = true;
                requestCameraPermissionFromUser();
                break;
            }
            case R.id.btnGallery:{
                isCameraPressed = false;
                requestCameraPermissionFromUser();
                break;
            }
            case R.id.btnDone:{
                //callUploadImageAPI(adapter.images.get(0));
                //onMultipartUploadClick();
                boolean isNull = false;
                if(adapter == null){
                    isNull = true;
                }
                if(adapter != null) {
                    if (adapter.images == null) {
                        isNull = true;
                    }
                }
                ArrayList<String> imagesToSend = new ArrayList<>();
                Intent intent = new Intent();
                if(isNull){
                   imagesToSend = images;
                }else {
                    for (String image : adapter.images) {
                        imagesToSend.add(image);
                    }
                }
                intent.putExtra(IMAGES, imagesToSend);
                getActivity().setResult(Activity.RESULT_OK, intent);
                //getActivity().onBackPressed();
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                break;
            }
        }
    }



    private class ImagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        private LayoutInflater inflater;
        private List<String> images;

        public ImagesAdapter(List<String> images){
            this.inflater = LayoutInflater.from(getActivity());
            this.images = images;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = this.inflater.inflate(R.layout.image_item,parent,false);
            ImagesViewHolder imagesViewHolder = new ImagesViewHolder(v);
            return imagesViewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if(holder instanceof ImagesViewHolder){
                ImagesViewHolder imagesViewHolder = (ImagesViewHolder) holder;
                String imageUrl = images.get(position);
                if(imageUrl != null) {
                    imageLoadingController.loadImage(imagesViewHolder.imgPicture, imageUrl);
                }
            }
        }

        @Override
        public int getItemCount() {
            return images.size();
        }

        public class ImagesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            private ImageView imgPicture,imgRemove;

            public ImagesViewHolder(View itemView) {
                super(itemView);
                imgPicture = (ImageView) itemView.findViewById(R.id.imgPicture);
                imgRemove = (ImageView) itemView.findViewById(R.id.imgRemove);
                imgRemove.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.imgRemove:{
                        adapter.images.remove(getLayoutPosition());
                        adapter.notifyItemRemoved(getLayoutPosition());
                        break;
                    }
                }
            }
        }
    }



    public void pickImageSingle() {
        imagePicker = new ImagePicker(this);
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }

    public void pickImageMultiple() {
        imagePicker = new ImagePicker(this);
        imagePicker.allowMultiple();
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }

    public void takePicture() {
        cameraPicker = new CameraImagePicker(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        cameraPicker.setImagePickerCallback(this);
        pickerPath = cameraPicker.pickImage();
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        //imageLoadingController.loadImage(imgPicture,images.get(0).getOriginalPath());
        //addImagesToAdapter(images.get(0).getOriginalPath());
        deletePicture(images.get(0).getThumbnailPath());
        deletePicture(images.get(0).getThumbnailSmallPath());
        SessionManager.get(getActivity()).setImageFileURL(images.get(0).getOriginalPath());
        new CompressImage().execute();
    }

    private String getCommaSeperatedImageURLs(){
        String commaSeperated = "";
        for(int i=0;i<adapter.images.size();i++){
            commaSeperated += adapter.images.get(i);
            if(i != adapter.images.size() - 1){
                commaSeperated += ",";
            }
        }
        return commaSeperated;
    }

    @Override
    public void onError(String s) {
        Toast.makeText(getActivity(),s,Toast.LENGTH_LONG).show();
    }

    private void callUploadImageAPI(String imageUrl){
        File file;
        try {
            file = new File(imageUrl);
        }
        catch (Exception ex){
            ex.printStackTrace();
            return;
        }
        RequestBody photo = RequestBody.create(MediaType.parse("application/image"), file);
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("image", file.getName(), photo)
                .build();

        Call<ErrorResponse2> call = RestClient.get().getApiService().uploadImage(body,SessionManager.get(getActivity()).getUserId(), AppConstants.TOKEN_ONE,AppConstants.TOKEN_TWO);
        progressCustomDialogControllerUploadingImage.showDialog();
        call.enqueue(new Callback<ErrorResponse2>() {
            @Override
            public void onResponse(Call<ErrorResponse2> call, Response<ErrorResponse2> response) {
                if(getActivity() != null){
                    progressCustomDialogControllerUploadingImage.hideDialog();
                    if(response.isSuccessful()){
                        ErrorResponse2 responseMain = response.body();
                        if(responseMain != null){
                            if(responseMain.getStatus()){
                                // success
                                Toast.makeText(getActivity(),"success",Toast.LENGTH_LONG).show();
                            }else {
                                // failed
                                Toast.makeText(getActivity(),"failure",Toast.LENGTH_LONG).show();
                            }
                        }
                    }else {
                        ErrorResponse2 errorResponse = parseError(response);
                        if(errorResponse != null) {
                            if(errorResponse.getMessage() != null) {
                                snackUtil.showSnackBarLongTime(btnCamera, errorResponse.getMessage());
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ErrorResponse2> call, Throwable t) {
                if(getActivity() != null){
                    progressCustomDialogControllerUploadingImage.hideDialog();
                    t.printStackTrace();
                }
            }
        });
    }

    public ErrorResponse2 parseError(Response<?> response) {
        Converter<ResponseBody, ErrorResponse2> converter =
                RestClient.get().getRetrofit()
                        .responseBodyConverter(ErrorResponse2.class, new Annotation[0]);
        ErrorResponse2 error;
        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ErrorResponse2();
        }

        return error;
    }



    private static final String USER_AGENT = "ICONUploadService/" + BuildConfig.VERSION_NAME;
    private Map<String, UploadProgressViewHolder> uploadProgressHolders = new HashMap<>();
    void onMultipartUploadClick() {
        String userId = String.valueOf(SessionManager.get(getActivity()).getUserId());
        final String serverUrlString = "http://iconapi.urwasoft.com/saveclaimpic/" + userId + "/"+ AppConstants.TOKEN_ONE +"/" + AppConstants.TOKEN_TWO;
        final String paramNameString = "image";

        final String filesToUploadString = getCommaSeperatedImageURLs();
        final String[] filesToUploadArray = filesToUploadString.split(",");

        /*try {
            MultipartUploadRequest req = new MultipartUploadRequest(getActivity(), serverUrlString)
                    .setNotificationConfig(getNotificationConfig())
                    .setCustomUserAgent(USER_AGENT)
                    .setMaxRetries(3);
            req.setUtf8Charset();
            for(int i=0;i<adapter.images.size();i++){
                req.addFileToUpload(adapter.images.get(i), paramNameString+String.valueOf(i));
            }
            String uploadID = req.setDelegate(this).startUpload();
            addUploadToList(uploadID,"HAHAHA");

        } catch (FileNotFoundException exc) {
            exc.printStackTrace();
        } catch (IllegalArgumentException exc) {
            exc.printStackTrace();
        } catch (MalformedURLException exc) {
            exc.printStackTrace();
        }*/



        // THIS IS FOR NEW NOTIFICATION FOR EACH FILE
        for (String fileToUploadPath : filesToUploadArray) {
            try {
                final String filename = getFilename(fileToUploadPath);

                MultipartUploadRequest req = new MultipartUploadRequest(getActivity(), serverUrlString)
                        .addFileToUpload(fileToUploadPath, paramNameString)
                        .setNotificationConfig(getNotificationConfig())
                        .setCustomUserAgent(USER_AGENT)
                        //.setAutoDeleteFilesAfterSuccessfulUpload(autoDeleteUploadedFiles.isChecked())
                        //.setUsesFixedLengthStreamingMode(fixedLengthStreamingMode.isChecked())
                        .setMaxRetries(3);
                    req.setUtf8Charset();
                String uploadID = req.setDelegate(this).startUpload();
                addUploadToList(uploadID,filename);

            } catch (FileNotFoundException exc) {
                exc.printStackTrace();
            } catch (IllegalArgumentException exc) {
                exc.printStackTrace();
            } catch (MalformedURLException exc) {
                exc.printStackTrace();
            }
        }
    }

    private UploadNotificationConfig getNotificationConfig() {
        //if (!displayNotification.isChecked()) return null;

        return new UploadNotificationConfig()
                .setIcon(R.drawable.ic_upload)
                .setCompletedIcon(R.drawable.ic_upload_success)
                .setErrorIcon(R.drawable.ic_upload_error)
                .setTitle(utils.getStringFromResourceId(R.string.uploading_survey_image))
                .setInProgressMessage(getString(R.string.uploading))
                .setCompletedMessage(getString(R.string.upload_success))
                .setErrorMessage(getString(R.string.upload_error))
                .setAutoClearOnSuccess(true)
                //.setClickIntent(new Intent(this, MainActivity.class))
                .setClearOnAction(true)
                .setRingToneEnabled(true);
    }

    private void addUploadToList(String uploadID, String filename) {
        View uploadProgressView = getActivity().getLayoutInflater().inflate(R.layout.view_upload_progress, null);
        UploadProgressViewHolder viewHolder = new UploadProgressViewHolder(uploadProgressView, filename);
        viewHolder.uploadId = uploadID;
        //container.addView(viewHolder.itemView, 0);
        uploadProgressHolders.put(uploadID, viewHolder);
    }

    private void logSuccessfullyUploadedFiles(List<String> files) {
        for (String file : files) {
            Log.e(TAG, "Success:" + file);
        }
    }

    private String getFilename(String filepath) {
        if (filepath == null)
            return null;

        final String[] filepathParts = filepath.split("/");

        return filepathParts[filepathParts.length - 1];
    }

    @Override
    public void onProgress(UploadInfo uploadInfo) {
        Log.i(TAG, String.format(Locale.getDefault(), "ID: %1$s (%2$d%%) at %3$.2f Kbit/s",
                uploadInfo.getUploadId(), uploadInfo.getProgressPercent(),
                uploadInfo.getUploadRate()));
        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());

        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
            return;

        uploadProgressHolders.get(uploadInfo.getUploadId())
                .progressBar.setProgress(uploadInfo.getProgressPercent());
    }

    @Override
    public void onError(UploadInfo uploadInfo, Exception exception) {
        Log.e(TAG, "Error with ID: " + uploadInfo.getUploadId() + ": "
                + exception.getLocalizedMessage(), exception);
        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());

        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
            return;

        //container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    @Override
    public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {
        Log.i(TAG, String.format(Locale.getDefault(),
                "ID %1$s: completed in %2$ds at %3$.2f Kbit/s. Response code: %4$d, body:[%5$s]",
                uploadInfo.getUploadId(), uploadInfo.getElapsedTime() / 1000,
                uploadInfo.getUploadRate(), serverResponse.getHttpCode(),
                serverResponse.getBodyAsString()));
        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());
        for (Map.Entry<String, String> header : serverResponse.getHeaders().entrySet()) {
            Log.i("Header", header.getKey() + ": " + header.getValue());
        }

        Log.e(TAG, "Printing response body bytes");
        byte[] ba = serverResponse.getBody();
        for (int j = 0; j < ba.length; j++) {
            Log.e(TAG, String.format("%02X ", ba[j]));
        }

        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
            return;

        //container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    @Override
    public void onCancelled(UploadInfo uploadInfo) {
        Log.i(TAG, "Upload with ID " + uploadInfo.getUploadId() + " is cancelled");
        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());

        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
            return;

        //container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    class UploadProgressViewHolder {
        View itemView;
        TextView uploadTitle;
        ProgressBar progressBar;

        String uploadId;

        UploadProgressViewHolder(View view, String filename) {
            itemView = view;
            uploadTitle = (TextView) itemView.findViewById(R.id.uploadTitle);
            progressBar = (ProgressBar) itemView.findViewById(R.id.uploadProgress);

            progressBar.setMax(100);
            progressBar.setProgress(0);

            uploadTitle.setText(getString(R.string.upload_progress, filename));
        }
    }
























    private void captureImage(){
        File file = getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if(file != null) {
            imageUrlMain = file.getAbsolutePath();
            SessionManager.get(getActivity()).setImageFileURL(imageUrlMain);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri fileUri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                fileUri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);
            } else {
                fileUri = Uri.fromFile(file);
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        }else {
            snackUtil.showSnackBarLongTime(btnCamera,R.string.unable_to_create_image_directory);
        }

        /*if(file != null) {
            imageUrlMain = file.getAbsolutePath();
            Intent i = new CameraActivity.IntentBuilder(getActivity())
                    .skipConfirm()
                    .skipOrientationNormalization()
                    .facing(Facing.BACK)
                    .to(file)
                    .debug()
                    .zoomStyle(ZoomStyle.SEEKBAR)
                    .updateMediaStore()
                    .build();
            startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        }else {
            snackUtil.showSnackBarLongTime(btnCamera,R.string.unable_to_create_image_directory);
        }*/
    }


    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final String IMAGE_DIRECTORY_NAME = "ICON Photos";
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("IZHALHA", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                if (resultCode == getActivity().RESULT_OK) {
                    new CompressImage().execute();
                }
            }else
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                    imagePicker.setImagePickerCallback(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.setImagePickerCallback(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
        }
    }

    private class CompressImage extends AsyncTask<Void,Void,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!(getActivity().isFinishing())) {
                progressCustomDialogControllerPleaseWait.showDialog();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            //String savedPath = imageUrlMain;
            String savedPath = SessionManager.get(getActivity()).getImageFileURL();
            //Bitmap bitmap = getCompressedBitmap(savedPath);
            Bitmap bitmap = getCompressedBitmapMoreOptimized(savedPath);
            if(bitmap != null){
                String newPath = savePhoto(bitmap);
                bitmap.recycle();
                return newPath;
            }else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressCustomDialogControllerPleaseWait.hideDialog();
            //imageLoadingController.loadImage(imgPicture,s);
            addImagesToAdapter(s);
        }
    }

    /**
     * Displaying captured image/video on the screen
     */
    private Bitmap getCompressedBitmap(String filePath) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
            //return bitmap;
            ExifInterface ei = new ExifInterface(filePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateBitmap(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateBitmap(bitmap, 180);
                    break;
            }
            File oldImageFile = new File(filePath);
            oldImageFile.delete();
            refreshGallery(oldImageFile.getAbsolutePath());
            return bitmap;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    private Bitmap getCompressedBitmapMoreOptimized(String imagePath) {
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];
        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.RGB_565);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        if (bmp != null) {
            bmp.recycle();
        }
        ExifInterface exif;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            File oldImageFile = new File(imagePath);
            oldImageFile.delete();
            refreshGallery(oldImageFile.getAbsolutePath());
            return scaledBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        /*FileOutputStream out = null;
        String filepath = MainActivity.imageFilePath;//getFilename();
        try {
            //new File(imageFilePath).delete();
            out = new FileOutputStream(filepath);

            //write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filepath;*/
    }

    private static final float maxHeight = 1280.0f;
    private static final float maxWidth = 1280.0f;

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    private void deletePicture(String filePath){
        try {
            File oldImageFile = new File(filePath);
            oldImageFile.delete();
            refreshGallery(oldImageFile.getAbsolutePath());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private Bitmap rotateBitmap(Bitmap bmp, int degree)
    {
        Matrix mat = new Matrix();
        mat.postRotate(degree);
        Bitmap bmpRotate = Bitmap.createBitmap(bmp, 0, 0,
                bmp.getWidth(), bmp.getHeight(), mat, true);
        bmp.recycle();
        return bmpRotate;
    }

    public String savePhoto(Bitmap bmp)
    {
        FileOutputStream out = null;
        try
        {
            // Create a media file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            File imageFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            out = new FileOutputStream(imageFile);
            bmp.compress(Bitmap.CompressFormat.JPEG, 80, out);
            out.flush();
            out.close();
            refreshGallery(imageFile.getAbsolutePath());

            out = null;
            return imageFile.getAbsolutePath();
        } catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
    }

    private void refreshGallery(String filePath){
        //refresh gallery to show new saved image
        MediaScannerConnection
                .scanFile(
                        getActivity(),
                        new String[]{filePath},
                        null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(
                                    String path, Uri uri) {
                                Log.i("ExternalStorage", "Scanned "
                                        + path + ":");
                                Log.i("ExternalStorage", "-> uri="
                                        + uri);
                            }
                        });
    }







    final private int REQUEST_CODE_ASK_PERMISSIONS = 125;
    private boolean mPermissionDenied = false;
    private void requestCameraPermissionFromUser() {
        int hasCameraPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        int hasReadPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int hasWritePermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if ( (hasCameraPermission != PackageManager.PERMISSION_GRANTED) ||
                (hasReadPermission != PackageManager.PERMISSION_GRANTED) ||
                (hasWritePermission != PackageManager.PERMISSION_GRANTED)
                ) {
            requestPermissions(
                    new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    },
                    REQUEST_CODE_ASK_PERMISSIONS);
            return;
        } else {
            if(isCameraPressed) {
                captureImage();
            }else {
                pickImageSingle();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if ((grantResults[0] == PackageManager.PERMISSION_GRANTED) &&
                        (grantResults[1] == PackageManager.PERMISSION_GRANTED) &&
                        (grantResults[2] == PackageManager.PERMISSION_GRANTED)
                        ) {
                    // Permission Allowed
                    int hasCameraPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
                    int hasReadPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
                    int hasWritePermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if ( (hasCameraPermission == PackageManager.PERMISSION_GRANTED) &&
                            (hasReadPermission == PackageManager.PERMISSION_GRANTED) &&
                            (hasWritePermission == PackageManager.PERMISSION_GRANTED)
                            ) {
                        if(isCameraPressed) {
                            captureImage();
                        }else {
                            pickImageSingle();
                        }
                    }
                } else {
                    // Permission Denied
                    mPermissionDenied = true;
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
        else {
            //requestCameraPermissionFromUser();
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getActivity().getSupportFragmentManager(), "dialog");
    }
}


































