package com.urwasoft.icon.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.urwasoft.icon.BuildConfig;
import com.urwasoft.icon.R;
import com.urwasoft.icon.activity.PhotosActivity;
import com.urwasoft.icon.activity.SurveyDetailActivity;
import com.urwasoft.icon.controller.ProgressCustomDialogController;
import com.urwasoft.icon.rest.RestClient;
import com.urwasoft.icon.rest.request.SubmitRequest;
import com.urwasoft.icon.rest.response.ErrorResponse2;
import com.urwasoft.icon.rest.response.SurveyResponse;
import com.urwasoft.icon.util.ActionEditText;
import com.urwasoft.icon.util.AppConstants;
import com.urwasoft.icon.util.SessionManager;
import com.urwasoft.icon.util.SnackUtil;
import com.urwasoft.icon.util.Utils;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import static com.facebook.GraphRequest.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class SurveyDetailFragment extends Fragment implements View.OnClickListener,UploadStatusDelegate,DatePickerFragment.DateSelectedListener {
    public SurveyResponse surveyResponse;
    //private TextInputEditText edtTotalCost,edtSurveyDetails;
    private EditText edtTotalCost2,edtRegistrationYear,edtAssembled,edtTransmission,edtMarketValue,edtMissingTerms,edtInsuredAddress,edtAccessories;
    private ActionEditText edtBodyObservation;
    private TextInputEditText edtSurveyDetails;
    private Button btnSubmit,btnAddPhotos;
    private Utils utils;
    private SnackUtil snackUtil;
    private ProgressCustomDialogController progressCustomDialogControllerUpdatingTotalCost;
    private TextView txtMake,txtModel,txtRegNo,txtInsuranceCompany,txtRepairer,txtContact,txtContactNumber,txtContactName;
    private TextView txtEngineNumber,txtChasisNumber,txtLicenseNumber,txtDriverName,txtPolicyNumber,txtOurReference,txtPolicyFromDate,txtPolicyToDate;
    private TextView txtSurveyorName,txtTotalCost,txtSumInsured,txtLossNumber,txtAddedOn,txtReportDate,txtIssueDate,txtExpiryDate,txtDateOfLoss,txtDateOfIntimation,txtSurveyDate,txtRegistrationDate;
    private ArrayList<String> images;
    private EditText edtMake,edtModel,edtRegNo,edtRepairer,edtEngineNo,edtChasisNo,edtDriverName,edtLossNumber;
    public boolean isSurveyUpdated = false;

    private EditText edtInsuranceCompany,edtContactNumber,edtContactName,edtLicenseNumber,edtPolicyNumber,edtSumInsured;
    private EditText edtSuveyorName;

    private TextView txtTempForDate;

    private int selectedHour = 0,selectedMinute = 0,selectedYear = 0,selectedMonth = 0,selectedDay = 0;

    public static final int IMAGES_REQUEST_CODE = 123;

    public SurveyDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        surveyResponse = getActivity().getIntent().getParcelableExtra(SurveyDetailActivity.SURVEY_RESPONSE);
        images = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_survey_detail, container, false);

        initializeViews(v);

        return v;
    }

    private void initializeViews(View v){
        //edtTotalCost = (TextInputEditText) v.findViewById(R.id.edtTotalCost);
        edtTotalCost2 = (EditText) v.findViewById(R.id.edtTotalCost2);
        edtSurveyDetails = (TextInputEditText) v.findViewById(R.id.edtSurveyDetails);
        edtMake = (EditText) v.findViewById(R.id.edtMake);
        edtModel = (EditText) v.findViewById(R.id.edtModel);
        edtRegNo = (EditText) v.findViewById(R.id.edtRegNo);
        edtRepairer = (EditText) v.findViewById(R.id.edtRepairer);
        edtEngineNo = (EditText) v.findViewById(R.id.edtEngineNo);
        edtChasisNo = (EditText) v.findViewById(R.id.edtChasisNo);
        edtDriverName = (EditText) v.findViewById(R.id.edtDriverName);
        edtLossNumber = (EditText) v.findViewById(R.id.edtLossNumber);
        edtRegistrationYear = (EditText) v.findViewById(R.id.edtRegistrationYear);
        edtAssembled = (EditText) v.findViewById(R.id.edtAssembled);
        edtTransmission = (EditText) v.findViewById(R.id.edtTransmission);
        edtMarketValue = (EditText) v.findViewById(R.id.edtMarketValue);
        edtMissingTerms = (EditText) v.findViewById(R.id.edtMissingTerms);
        edtBodyObservation = (ActionEditText) v.findViewById(R.id.edtBodyObservation);
        edtAccessories = (EditText) v.findViewById(R.id.edtAccessories);
        edtInsuredAddress = (EditText) v.findViewById(R.id.edtInsuredAddress);
        edtInsuranceCompany = (EditText) v.findViewById(R.id.edtInsuranceCompany);
        edtContactNumber = (EditText) v.findViewById(R.id.edtContactNumber);
        edtContactName = (EditText) v.findViewById(R.id.edtContactName);
        edtLicenseNumber = (EditText) v.findViewById(R.id.edtLicenseNumber);
        edtPolicyNumber = (EditText) v.findViewById(R.id.edtPolicyNumber);
        edtSumInsured = (EditText) v.findViewById(R.id.edtSumInsured);
        edtSuveyorName = (EditText) v.findViewById(R.id.edtSuveyorName);

        btnSubmit = (Button) v.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        btnAddPhotos = (Button) v.findViewById(R.id.btnAddPhotos);
        btnAddPhotos.setOnClickListener(this);
        txtMake = (TextView) v.findViewById(R.id.txtMake);
        txtModel = (TextView) v.findViewById(R.id.txtModel);
        txtRegNo = (TextView) v.findViewById(R.id.txtRegNo);
        txtInsuranceCompany = (TextView) v.findViewById(R.id.txtInsuranceCompany);
        txtRepairer = (TextView) v.findViewById(R.id.txtRepairer);
        txtContact = (TextView) v.findViewById(R.id.txtContact);
        txtContactName = (TextView) v.findViewById(R.id.txtContactName);
        txtContactNumber = (TextView) v.findViewById(R.id.txtContactNumber);
        txtEngineNumber = (TextView) v.findViewById(R.id.txtEngineNumber);
        txtChasisNumber = (TextView) v.findViewById(R.id.txtChasisNumber);
        txtLicenseNumber = (TextView) v.findViewById(R.id.txtLicenseNumber);
        txtDriverName = (TextView) v.findViewById(R.id.txtDriverName);
        txtPolicyNumber = (TextView) v.findViewById(R.id.txtPolicyNumber);
        txtOurReference = (TextView) v.findViewById(R.id.txtOurReference);
        txtPolicyFromDate = (TextView) v.findViewById(R.id.txtPolicyFromDate);
        txtPolicyFromDate.setOnClickListener(this);
        txtPolicyToDate = (TextView) v.findViewById(R.id.txtPolicyToDate);
        txtPolicyToDate.setOnClickListener(this);

        txtSurveyorName = (TextView) v.findViewById(R.id.txtSurveyorName);
        txtTotalCost = (TextView) v.findViewById(R.id.txtTotalCost);
        txtSumInsured = (TextView) v.findViewById(R.id.txtSumInsured);
        txtLossNumber = (TextView) v.findViewById(R.id.txtLossNumber);

        txtAddedOn = (TextView) v.findViewById(R.id.txtAddedOn);
        txtAddedOn.setOnClickListener(this);
        txtReportDate = (TextView) v.findViewById(R.id.txtReportDate);
        txtReportDate.setOnClickListener(this);
        txtIssueDate = (TextView) v.findViewById(R.id.txtIssueDate);
        txtIssueDate.setOnClickListener(this);
        txtExpiryDate = (TextView) v.findViewById(R.id.txtExpiryDate);
        txtExpiryDate.setOnClickListener(this);
        txtDateOfLoss = (TextView) v.findViewById(R.id.txtDateOfLoss);
        txtDateOfLoss.setOnClickListener(this);
        txtDateOfIntimation = (TextView) v.findViewById(R.id.txtDateOfIntimation);
        txtDateOfIntimation.setOnClickListener(this);
        txtSurveyDate = (TextView) v.findViewById(R.id.txtSurveyDate);
        txtRegistrationDate = (TextView) v.findViewById(R.id.txtRegistrationDate);
        txtRegistrationDate.setOnClickListener(this);

        utils = new Utils(getActivity());
        snackUtil = new SnackUtil(getActivity());
        progressCustomDialogControllerUpdatingTotalCost = new ProgressCustomDialogController(getActivity(),R.string.updating_total_cost);

        setDetails();
    }

    private void setDetails(){
        if(surveyResponse != null){
            if((surveyResponse.getMake() != null) && (!surveyResponse.getMake().equals(""))){
                txtMake.setText(surveyResponse.getMake());
                edtMake.setText(surveyResponse.getMake());
            }
            if((surveyResponse.getMake() != null) && (!surveyResponse.getMake().equals(""))){
                txtModel.setText(surveyResponse.getModel());
                edtModel.setText(surveyResponse.getModel());
            }
            if((surveyResponse.getRegistrationNumber() != null) && (!surveyResponse.getRegistrationNumber().equals(""))){
                txtRegNo.setText(surveyResponse.getRegistrationNumber());
                edtRegNo.setText(surveyResponse.getRegistrationNumber());
            }
            if((surveyResponse.getInsuranceCompanyName() != null) && (!surveyResponse.getInsuranceCompanyName().equals(""))){
                txtInsuranceCompany.setText(surveyResponse.getInsuranceCompanyName());
                edtInsuranceCompany.setText(surveyResponse.getInsuranceCompanyName());
            }
            if((surveyResponse.getRepairer() != null) && (!surveyResponse.getRepairer().equals(""))){
                txtRepairer.setText(surveyResponse.getRepairer());
                edtRepairer.setText(surveyResponse.getRepairer());
            }
            if((surveyResponse.getContactForSurvey() != null) && (!surveyResponse.getContactForSurvey().equals(""))){
                txtContact.setText(surveyResponse.getContactForSurvey());
            }
            if((surveyResponse.getContactNumber() != null) && (!surveyResponse.getContactNumber().equals(""))){
                txtContactNumber.setText(surveyResponse.getContactNumber());
                edtContactNumber.setText(surveyResponse.getContactNumber());
            }
            if((surveyResponse.getContactName() != null) && (!surveyResponse.getContactName().equals(""))){
                txtContactName.setText(surveyResponse.getContactName());
                edtContactName.setText(surveyResponse.getContactName());
            }
            if((surveyResponse.getEnginenumber() != null) && (!surveyResponse.getEnginenumber().equals(""))){
                txtEngineNumber.setText(surveyResponse.getEnginenumber());
                edtEngineNo.setText(surveyResponse.getEnginenumber());
            }
            if((surveyResponse.getChasisnumber() != null) && (!surveyResponse.getChasisnumber().equals(""))){
                txtChasisNumber.setText(surveyResponse.getChasisnumber());
                edtChasisNo.setText(surveyResponse.getChasisnumber());
            }
            if((surveyResponse.getLicensenumber() != null) && (!surveyResponse.getLicensenumber().equals(""))){
                txtLicenseNumber.setText(surveyResponse.getLicensenumber());
                edtLicenseNumber.setText(surveyResponse.getLicensenumber());
            }
            if((surveyResponse.getDriverName() != null) && (!surveyResponse.getDriverName().equals(""))){
                txtDriverName.setText(surveyResponse.getDriverName());
                edtDriverName.setText(surveyResponse.getDriverName());
            }
            if((surveyResponse.getPolicyNumber() != null) && (!surveyResponse.getPolicyNumber().equals(""))){
                txtPolicyNumber.setText(surveyResponse.getPolicyNumber());
                edtPolicyNumber.setText(surveyResponse.getPolicyNumber());
            }
            if((surveyResponse.getOurRef() != null) && (!surveyResponse.getOurRef().equals(""))){
                txtOurReference.setText(surveyResponse.getOurRef());
            }
            if((surveyResponse.getSurveyorName() != null) && (!surveyResponse.getSurveyorName().equals(""))){
                txtSurveyorName.setText(surveyResponse.getSurveyorName());
                edtSuveyorName.setText(surveyResponse.getSurveyorName());
            }
            if((surveyResponse.getTotalCost() != null) && (!surveyResponse.getTotalCost().equals(""))){
                edtTotalCost2.setText(String.valueOf(surveyResponse.getTotalCost()));
            }
            if((surveyResponse.getSumInsured() != null) && (!surveyResponse.getSumInsured().equals(""))){
                txtSumInsured.setText(String.valueOf(surveyResponse.getSumInsured()));
                edtSumInsured.setText(String.valueOf(surveyResponse.getSumInsured()));
            }
            if((surveyResponse.getLossNumber() != null) && (!surveyResponse.getLossNumber().equals(""))){
                txtLossNumber.setText(surveyResponse.getLossNumber());
                edtLossNumber.setText(surveyResponse.getLossNumber());
            }

            try {
                if ((surveyResponse.getPolicyPeriodFrom() != null) && (!surveyResponse.getPolicyPeriodFrom().equals(""))){
                    txtPolicyFromDate.setText(utils.getFormattedDateByDotNetDateFormat(surveyResponse.getPolicyPeriodFrom()));
                }
                if( (surveyResponse.getPolicyPeriodTo() != null)  && (!surveyResponse.getPolicyPeriodTo().equals(""))){
                    txtPolicyToDate.setText(utils.getFormattedDateByDotNetDateFormat(surveyResponse.getPolicyPeriodTo()));
                }
                if ((surveyResponse.getAddedOn() != null)  && (!surveyResponse.getAddedOn().equals(""))){
                    txtAddedOn.setText(utils.getFormattedDateByDotNetDateFormat(surveyResponse.getAddedOn()));
                }
                if ((surveyResponse.getReportdate() != null)  && (!surveyResponse.getReportdate().equals(""))){
                    txtReportDate.setText(utils.getFormattedDateByDotNetDateFormat(surveyResponse.getReportdate()));
                }
                if ((surveyResponse.getIssueDate() != null)  && (!surveyResponse.getIssueDate().equals(""))){
                    txtIssueDate.setText(utils.getFormattedDateByDotNetDateFormat(surveyResponse.getIssueDate()));
                }
                if ((surveyResponse.getExpiry() != null)  && (!surveyResponse.getExpiry().equals(""))){
                    txtExpiryDate.setText(utils.getFormattedDateByDotNetDateFormat(surveyResponse.getExpiry()));
                }
                if ((surveyResponse.getDateofLoss() != null)  && (!surveyResponse.getDateofLoss().equals(""))){
                    txtDateOfLoss.setText(utils.getFormattedDateByDotNetDateFormat(surveyResponse.getDateofLoss()));
                }
                if ((surveyResponse.getDateofintimation() != null)  && (!surveyResponse.getDateofintimation().equals(""))){
                    txtDateOfIntimation.setText(utils.getFormattedDateByDotNetDateFormat(surveyResponse.getDateofintimation()));
                }
                if ((surveyResponse.getSurveyDate() != null)  && (!surveyResponse.getSurveyDate().equals(""))){
                    txtSurveyDate.setText(utils.getFormattedDateByDotNetDateFormat(surveyResponse.getSurveyDate()));
                    txtAddedOn.setText(utils.getFormattedDateByDotNetDateFormat(surveyResponse.getSurveyDate()));
                }
                if ((surveyResponse.getRegistrationDate() != null)  && (!surveyResponse.getRegistrationDate().equals(""))){
                    txtRegistrationDate.setText(utils.getFormattedDateByDotNetDateFormat(surveyResponse.getRegistrationDate()));
                }
                if(surveyResponse.getRegistrationYear() != null){
                    edtRegistrationYear.setText(surveyResponse.getRegistrationYear());
                }
                if(surveyResponse.getAssembled() != null){
                    edtAssembled.setText(surveyResponse.getAssembled());
                }
                if(surveyResponse.getTransmission() != null){
                    edtTransmission.setText(surveyResponse.getTransmission());
                }
                if(surveyResponse.getMarketValue() != null){
                    edtMarketValue.setText(surveyResponse.getMarketValue());
                }
                if(surveyResponse.getMissingTerms() != null){
                    edtMissingTerms.setText(surveyResponse.getMissingTerms());
                }
                if(surveyResponse.getBodyObservation() != null){
                    edtBodyObservation.setText(surveyResponse.getBodyObservation());
                }
                if(surveyResponse.getAccessories() != null){
                    edtAccessories.setText(surveyResponse.getAccessories());
                }
                if(surveyResponse.getInsuredAddress() != null){
                    edtInsuredAddress.setText(surveyResponse.getInsuredAddress());
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSubmit:{
                /*if((!(utils.isEditTextNullOrEmpty(edtTotalCost))) && (images.size() != 0)){
                    double price = Double.parseDouble(edtTotalCost.getText().toString());
                    callSubmitTotalCost(surveyResponse.getIdx(), price, AppConstants.TOKEN_ONE, AppConstants.TOKEN_TWO);
                }else if(images.size() != 0){
                    Toast.makeText(getActivity(), utils.getStringFromResourceId(R.string.uploading_pictures_in_bg), Toast.LENGTH_LONG).show();
                    onMultipartUploadClick();
                }else if(!(utils.isEditTextNullOrEmpty(edtTotalCost))){
                    double price = Double.parseDouble(edtTotalCost.getText().toString());
                    callSubmitTotalCost(surveyResponse.getIdx(), price, AppConstants.TOKEN_ONE, AppConstants.TOKEN_TWO);
                }else {
                    Toast.makeText(getActivity(), utils.getStringFromResourceId(R.string.please_select_photos_or_total_cost), Toast.LENGTH_LONG).show();
                }*/
                if(!utils.isInternetAvailable()){
                    snackUtil.showSnackBarLongTime(btnSubmit,R.string.please_check_your_internet_connection);
                    return;
                }
                callSubmitTotalCost(surveyResponse.getIdx(), getSubmitRequest(), AppConstants.TOKEN_ONE, AppConstants.TOKEN_TWO);
                break;
            }
            case R.id.btnAddPhotos:{
                Intent intent = new Intent(getActivity(), PhotosActivity.class);
                intent.putStringArrayListExtra(PhotosFragment.IMAGES,images);
                startActivityForResult(intent,IMAGES_REQUEST_CODE);
                break;
            }
            case R.id.txtPolicyFromDate:{
                txtTempForDate = txtPolicyFromDate;
                DialogFragment newFragment = new DatePickerFragment(SurveyDetailFragment.this,selectedYear,selectedMonth,selectedDay);
                newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
                break;
            }
            case R.id.txtPolicyToDate:{
                txtTempForDate = txtPolicyToDate;
                DialogFragment newFragment = new DatePickerFragment(SurveyDetailFragment.this,selectedYear,selectedMonth,selectedDay);
                newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
                break;
            }

            case R.id.txtIssueDate:{
                txtTempForDate = txtIssueDate;
                DialogFragment newFragment = new DatePickerFragment(SurveyDetailFragment.this,selectedYear,selectedMonth,selectedDay);
                newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
                break;
            }
            case R.id.txtExpiryDate:{
                txtTempForDate = txtExpiryDate;
                DialogFragment newFragment = new DatePickerFragment(SurveyDetailFragment.this,selectedYear,selectedMonth,selectedDay);
                newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
                break;
            }
            case R.id.txtDateOfLoss:{
                txtTempForDate = txtDateOfLoss;
                DialogFragment newFragment = new DatePickerFragment(SurveyDetailFragment.this,selectedYear,selectedMonth,selectedDay);
                newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
                break;
            }
            case R.id.txtRegistrationDate:{
                txtTempForDate = txtRegistrationDate;
                DialogFragment newFragment = new DatePickerFragment(SurveyDetailFragment.this,selectedYear,selectedMonth,selectedDay);
                newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
                break;
            }
            case R.id.txtAddedOn:{
                txtTempForDate = txtAddedOn;
                DialogFragment newFragment = new DatePickerFragment(SurveyDetailFragment.this,selectedYear,selectedMonth,selectedDay);
                newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
                break;
            }
            case R.id.txtReportDate:{
                txtTempForDate = txtReportDate;
                DialogFragment newFragment = new DatePickerFragment(SurveyDetailFragment.this,selectedYear,selectedMonth,selectedDay);
                newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
                break;
            }
            case R.id.txtDateOfIntimation:{
                txtTempForDate = txtDateOfIntimation;
                DialogFragment newFragment = new DatePickerFragment(SurveyDetailFragment.this,selectedYear,selectedMonth,selectedDay);
                newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGES_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                //vendorId = data.getIntExtra(KEY_VENDOR_ID,-1);
                //txtSelectVendor.setText(data.getStringExtra(KEY_VENDOR_NAME));
                if(data != null){
                    if(data.getStringArrayListExtra(PhotosFragment.IMAGES) != null) {
                        images = data.getStringArrayListExtra(PhotosFragment.IMAGES);
                    }else {
                        images = new ArrayList<>();
                    }
                }
            }
        }
    }

    public void callSubmitTotalCost(int surveyId, final SubmitRequest submitRequest, String token1, String token2){
        Call<ErrorResponse2> call = RestClient.get().getApiService().submitTotalCost(surveyId,submitRequest,token1,token2);
        progressCustomDialogControllerUpdatingTotalCost.showDialog();
        call.enqueue(new Callback<ErrorResponse2>() {
            @Override
            public void onResponse(Call<ErrorResponse2> call, Response<ErrorResponse2> response) {
                if(getActivity() != null){
                    progressCustomDialogControllerUpdatingTotalCost.hideDialog();
                    if(response.isSuccessful()){
                        ErrorResponse2 responseMain = response.body();
                        if(responseMain != null){
                            if(responseMain.getStatus()){
                                // success
                                /*if(!(utils.isEditTextNullOrEmpty(edtTotalCost))) {
                                    txtTotalCost.setText(String.valueOf(edtTotalCost.getText().toString()));
                                }
                                edtTotalCost.setText("");*/
                                edtSurveyDetails.setText("");
                                Toast.makeText(getActivity(),utils.getStringFromResourceId(R.string.survey_updated),Toast.LENGTH_SHORT).show();
                                isSurveyUpdated = true;
                                updateSurveyObjectForHomeScreen(submitRequest);
                                if(images.size() != 0) {
                                    Toast.makeText(getActivity(), utils.getStringFromResourceId(R.string.uploading_pictures_in_bg), Toast.LENGTH_SHORT).show();
                                    onMultipartUploadClick();
                                }
                            }else {
                                // failed
                                isSurveyUpdated = false;
                                Toast.makeText(getActivity(),utils.getStringFromResourceId(R.string.failed_to_update_total_cost),Toast.LENGTH_LONG).show();
                            }
                        }
                    }else {
                        ErrorResponse2 errorResponse = parseError(response);
                        if(errorResponse != null) {
                            if(errorResponse.getMessage() != null) {
                                snackUtil.showSnackBarLongTime(btnSubmit, errorResponse.getMessage());
                                isSurveyUpdated = false;
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ErrorResponse2> call, Throwable t) {
                if(getActivity() != null){
                    isSurveyUpdated = false;
                    progressCustomDialogControllerUpdatingTotalCost.hideDialog();
                    t.printStackTrace();
                }
            }
        });
    }

    public ErrorResponse2 parseError(Response<?> response) {
        Converter<ResponseBody, ErrorResponse2> converter =
                RestClient.get().getRetrofit()
                        .responseBodyConverter(ErrorResponse2.class, new Annotation[0]);
        ErrorResponse2 error;
        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ErrorResponse2();
        }

        return error;
    }

    private SubmitRequest getSubmitRequest(){
        SubmitRequest submitRequest = new SubmitRequest();

        if(utils.isEditTextNullOrEmpty(edtMake)){
            submitRequest.setMake("");
        }else {
            submitRequest.setMake(edtMake.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtModel)){
            submitRequest.setModel("");
        }else {
            submitRequest.setModel(edtModel.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtRegNo)){
            submitRequest.setRegNo("");
        }else {
            submitRequest.setRegNo(edtRegNo.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtRepairer)){
            submitRequest.setRepairer("");
        }else {
            submitRequest.setRepairer(edtRepairer.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtEngineNo)){
            submitRequest.setEngineNumber("");
        }else {
            submitRequest.setEngineNumber(edtEngineNo.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtChasisNo)){
            submitRequest.setChasisNo("");
        }else {
            submitRequest.setChasisNo(edtChasisNo.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtDriverName)){
            submitRequest.setDriverName("");
        }else {
            submitRequest.setDriverName(edtDriverName.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtLossNumber)){
            submitRequest.setLossNo("");
        }else {
            submitRequest.setLossNo(edtLossNumber.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtSurveyDetails)){
            submitRequest.setSurveyDetails("");
        }else {
            submitRequest.setSurveyDetails(edtSurveyDetails.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtTotalCost2)){
            submitRequest.setTotalCost(0.0);
        }else {
            submitRequest.setTotalCost(Double.parseDouble(edtTotalCost2.getText().toString()));
        }
        submitRequest.setUserId(SessionManager.get(getActivity()).getUserId());

        if(utils.isEditTextNullOrEmpty(edtRegistrationYear)){
            submitRequest.setRegistrationYear("");
        }else {
            submitRequest.setRegistrationYear(edtRegistrationYear.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtAssembled)){
            submitRequest.setAssembled("");
        }else {
            submitRequest.setAssembled(edtAssembled.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtTransmission)){
            submitRequest.setTransmission("");
        }else {
            submitRequest.setTransmission(edtTransmission.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtMarketValue)){
            submitRequest.setMarketValue("");
        }else {
            submitRequest.setMarketValue(edtMarketValue.getText().toString());
        }

        if(utils.isEditTextNullOrEmpty(edtMissingTerms)){
            submitRequest.setMissingTerms("");
        }else {
            submitRequest.setMissingTerms(edtMissingTerms.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtBodyObservation)){
            submitRequest.setBodyObservation("");
        }else {
            submitRequest.setBodyObservation(edtBodyObservation.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtAccessories)){
            submitRequest.setAccessories("");
        }else {
            submitRequest.setAccessories(edtAccessories.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtInsuredAddress)){
            submitRequest.setInsuredAddress("");
        }else {
            submitRequest.setInsuredAddress(edtInsuredAddress.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtInsuranceCompany)){
            submitRequest.setInsuranceCompany("");
        }else {
            submitRequest.setInsuranceCompany(edtInsuranceCompany.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtContactNumber)){
            submitRequest.setContactNumber("");
        }else {
            submitRequest.setContactNumber(edtContactNumber.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtContactName)){
            submitRequest.setContactName("");
        }else {
            submitRequest.setContactName(edtContactName.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtLicenseNumber)){
            submitRequest.setLicenseNumber("");
        }else {
            submitRequest.setLicenseNumber(edtLicenseNumber.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtPolicyNumber)){
            submitRequest.setPolicyNumber("");
        }else {
            submitRequest.setPolicyNumber(edtPolicyNumber.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtSumInsured)){
            submitRequest.setSumInsured("");
        }else {
            submitRequest.setSumInsured(edtSumInsured.getText().toString());
        }
        if(utils.isEditTextNullOrEmpty(edtSuveyorName)){
            submitRequest.setSuveyorName("");
        }else {
            submitRequest.setSuveyorName(edtSuveyorName.getText().toString());
        }
        try {
            if(txtPolicyFromDate.getText().toString().length() < 8){
                submitRequest.setPolicyFromDate("");
            }else {
                submitRequest.setPolicyFromDate(utils.
                        getFormattedDate(
                                txtPolicyFromDate.getText().toString(),
                                AppConstants.DATE_FORMAT_SIX,
                                AppConstants.DATE_FORMAT_SEVEN
                                ));
            }
            if(txtPolicyToDate.getText().toString().length() < 8){
                submitRequest.setPolicyToDate("");
            }else {
                submitRequest.setPolicyToDate(utils.
                        getFormattedDate(
                                txtPolicyToDate.getText().toString(),
                                AppConstants.DATE_FORMAT_SIX,
                                AppConstants.DATE_FORMAT_SEVEN
                        ));
            }
            if(txtAddedOn.getText().toString().length() < 8){
                submitRequest.setSurveyDate("");
            }else {
                submitRequest.setSurveyDate(utils.
                        getFormattedDate(
                                txtAddedOn.getText().toString(),
                                AppConstants.DATE_FORMAT_SIX,
                                AppConstants.DATE_FORMAT_SEVEN
                        ));
            }
            if(txtReportDate.getText().toString().length() < 8){
                submitRequest.setReportDate("");
            }else {
                submitRequest.setReportDate(utils.
                        getFormattedDate(
                                txtReportDate.getText().toString(),
                                AppConstants.DATE_FORMAT_SIX,
                                AppConstants.DATE_FORMAT_SEVEN
                        ));
            }
            if(txtIssueDate.getText().toString().length() < 8){
                submitRequest.setIssueDate("");
            }else {
                submitRequest.setIssueDate(utils.
                        getFormattedDate(
                                txtIssueDate.getText().toString(),
                                AppConstants.DATE_FORMAT_SIX,
                                AppConstants.DATE_FORMAT_SEVEN
                        ));
            }
            if(txtExpiryDate.getText().toString().length() < 8){
                submitRequest.setExpiryDate("");
            }else {
                submitRequest.setExpiryDate(utils.
                        getFormattedDate(
                                txtExpiryDate.getText().toString(),
                                AppConstants.DATE_FORMAT_SIX,
                                AppConstants.DATE_FORMAT_SEVEN
                        ));
            }
            if(txtDateOfLoss.getText().toString().length() < 8){
                submitRequest.setDateOfLoss("");
            }else {
                submitRequest.setDateOfLoss(utils.
                        getFormattedDate(
                                txtDateOfLoss.getText().toString(),
                                AppConstants.DATE_FORMAT_SIX,
                                AppConstants.DATE_FORMAT_SEVEN
                        ));
            }
            if(txtDateOfIntimation.getText().toString().length() < 8){
                submitRequest.setDateOfIntimation("");
            }else {
                submitRequest.setDateOfIntimation(utils.
                        getFormattedDate(
                                txtDateOfIntimation.getText().toString(),
                                AppConstants.DATE_FORMAT_SIX,
                                AppConstants.DATE_FORMAT_SEVEN
                        ));
            }
            if(txtRegistrationDate.getText().toString().length() < 8){
                submitRequest.setRegistrationDate("");
            }else {
                submitRequest.setRegistrationDate(utils.
                        getFormattedDate(
                                txtRegistrationDate.getText().toString(),
                                AppConstants.DATE_FORMAT_SIX,
                                AppConstants.DATE_FORMAT_SEVEN
                        ));
            }
        }catch (Exception ex){
            Toast.makeText(getActivity(),ex.getMessage(),Toast.LENGTH_LONG);
            ex.printStackTrace();
        }

        return submitRequest;
    }

    private void updateSurveyObjectForHomeScreen(SubmitRequest submitRequest){
        surveyResponse.setAddedOn(submitRequest.getSurveyDate());
        //surveyResponse.setAddedby(submitRequest.getAddedBy);
        surveyResponse.setPolicyNumber(submitRequest.getPolicyNumber());
        surveyResponse.setReportdate(submitRequest.getReportDate());
        //surveyResponse.setRegistrationNumber(submitRequest.getRegi);
        surveyResponse.setMake(submitRequest.getMake());
        surveyResponse.setModel(submitRequest.getModel());
        surveyResponse.setEnginenumber(submitRequest.getEngineNumber());
        surveyResponse.setChasisnumber(submitRequest.getChasisNo());
        surveyResponse.setDriverName(submitRequest.getDriverName());
        surveyResponse.setIssueDate(submitRequest.getIssueDate());
        surveyResponse.setLicensenumber(submitRequest.getLicenseNumber());
        surveyResponse.setExpiry(submitRequest.getExpiryDate());
        surveyResponse.setDateofLoss(submitRequest.getDateOfLoss());
        surveyResponse.setDateofintimation(submitRequest.getDateOfIntimation());
        surveyResponse.setSurveyDate(submitRequest.getSurveyDate());
        surveyResponse.setLossNumber(submitRequest.getLossNo());
        surveyResponse.setPolicyPeriodFrom(submitRequest.getPolicyFromDate());
        surveyResponse.setPolicyPeriodTo(submitRequest.getPolicyToDate());
        surveyResponse.setSumInsured(submitRequest.getSumInsured());
        surveyResponse.setRegistrationDate(submitRequest.getRegistrationDate());
        //surveyResponse.setAssignedTo(submitRequest.getAss);
        surveyResponse.setSurveyorName(submitRequest.getSuveyorName());
        surveyResponse.setInsuranceCompanyName(submitRequest.getInsuranceCompany());
        surveyResponse.setRepairer(submitRequest.getRepairer());
        //surveyResponse.setContactForSurvey(submitRequest.getCon);
        surveyResponse.setTotalCost(submitRequest.getTotalCost());
        surveyResponse.setContactName(submitRequest.getContactName());
        surveyResponse.setContactNumber(submitRequest.getContactNumber());
        surveyResponse.setRegistrationYear(submitRequest.getRegistrationYear());
        surveyResponse.setAssembled(submitRequest.getAssembled());
        surveyResponse.setTransmission(submitRequest.getTransmission());
        surveyResponse.setMarketValue(submitRequest.getMarketValue());
        surveyResponse.setMissingTerms(submitRequest.getMissingTerms());
        surveyResponse.setInsuredAddress(submitRequest.getInsuredAddress());
        surveyResponse.setBodyObservation(submitRequest.getBodyObservation());
        surveyResponse.setAccessories(submitRequest.getAccessories());
    }





    private static final String USER_AGENT = "ICONUploadService/" + BuildConfig.VERSION_NAME;
    private Map<String, UploadProgressViewHolder> uploadProgressHolders = new HashMap<>();
    void onMultipartUploadClick() {
        final String serverUrlString = AppConstants.BASE_URL_PRODUCTION + "/saveclaimpic/" + surveyResponse.getIdx() + "/"+ AppConstants.TOKEN_ONE +"/" + AppConstants.TOKEN_TWO;
        final String paramNameString = "image";

        // THIS IS FOR NEW NOTIFICATION FOR EACH FILE
        for (String fileToUploadPath : images) {
            try {
                final String filename = getFilename(fileToUploadPath);

                MultipartUploadRequest req = new MultipartUploadRequest(getActivity(), serverUrlString)
                        .addFileToUpload(fileToUploadPath, paramNameString)
                        .setNotificationConfig(getNotificationConfig())
                        .setCustomUserAgent(USER_AGENT)
                        //.setAutoDeleteFilesAfterSuccessfulUpload(autoDeleteUploadedFiles.isChecked())
                        //.setUsesFixedLengthStreamingMode(fixedLengthStreamingMode.isChecked())
                        .setMaxRetries(3);
                req.setUtf8Charset();
                String uploadID = req.setDelegate(this).startUpload();
                addUploadToList(uploadID,filename);

            } catch (FileNotFoundException exc) {
                exc.printStackTrace();
            } catch (IllegalArgumentException exc) {
                exc.printStackTrace();
            } catch (MalformedURLException exc) {
                exc.printStackTrace();
            }
        }

        images = new ArrayList<>();
    }

    private UploadNotificationConfig getNotificationConfig() {
        //if (!displayNotification.isChecked()) return null;

        return new UploadNotificationConfig()
                .setIcon(R.drawable.ic_upload)
                .setCompletedIcon(R.drawable.ic_upload_success)
                .setErrorIcon(R.drawable.ic_upload_error)
                .setTitle(utils.getStringFromResourceId(R.string.uploading_survey_image))
                .setInProgressMessage(getString(R.string.uploading))
                .setCompletedMessage(getString(R.string.upload_success))
                .setErrorMessage(getString(R.string.upload_error))
                .setAutoClearOnSuccess(true)
                //.setClickIntent(new Intent(this, MainActivity.class))
                .setClearOnAction(true)
                .setRingToneEnabled(true);
    }

    private void addUploadToList(String uploadID, String filename) {
        View uploadProgressView = getActivity().getLayoutInflater().inflate(R.layout.view_upload_progress, null);
        UploadProgressViewHolder viewHolder = new UploadProgressViewHolder(uploadProgressView, filename);
        viewHolder.uploadId = uploadID;
        //container.addView(viewHolder.itemView, 0);
        uploadProgressHolders.put(uploadID, viewHolder);
    }

    private void logSuccessfullyUploadedFiles(List<String> files) {
        for (String file : files) {
            Log.e(TAG, "Success:" + file);
        }
    }

    private String getFilename(String filepath) {
        if (filepath == null)
            return null;

        final String[] filepathParts = filepath.split("/");

        return filepathParts[filepathParts.length - 1];
    }

    @Override
    public void onProgress(UploadInfo uploadInfo) {
        Log.i(TAG, String.format(Locale.getDefault(), "ID: %1$s (%2$d%%) at %3$.2f Kbit/s",
                uploadInfo.getUploadId(), uploadInfo.getProgressPercent(),
                uploadInfo.getUploadRate()));
        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());

        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
            return;

        uploadProgressHolders.get(uploadInfo.getUploadId())
                .progressBar.setProgress(uploadInfo.getProgressPercent());
    }

    @Override
    public void onError(UploadInfo uploadInfo, Exception exception) {
        Log.e(TAG, "Error with ID: " + uploadInfo.getUploadId() + ": "
                + exception.getLocalizedMessage(), exception);
        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());

        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
            return;

        //container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    @Override
    public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {
        Log.i(TAG, String.format(Locale.getDefault(),
                "ID %1$s: completed in %2$ds at %3$.2f Kbit/s. Response code: %4$d, body:[%5$s]",
                uploadInfo.getUploadId(), uploadInfo.getElapsedTime() / 1000,
                uploadInfo.getUploadRate(), serverResponse.getHttpCode(),
                serverResponse.getBodyAsString()));
        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());
        for (Map.Entry<String, String> header : serverResponse.getHeaders().entrySet()) {
            Log.i("Header", header.getKey() + ": " + header.getValue());
        }

        Log.e(TAG, "Printing response body bytes");
        byte[] ba = serverResponse.getBody();
        for (int j = 0; j < ba.length; j++) {
            Log.e(TAG, String.format("%02X ", ba[j]));
        }

        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
            return;

        //container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    @Override
    public void onCancelled(UploadInfo uploadInfo) {
        Log.i(TAG, "Upload with ID " + uploadInfo.getUploadId() + " is cancelled");
        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());

        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
            return;

        //container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    class UploadProgressViewHolder {
        View itemView;
        TextView uploadTitle;
        ProgressBar progressBar;

        String uploadId;

        UploadProgressViewHolder(View view, String filename) {
            itemView = view;
            uploadTitle = (TextView) itemView.findViewById(R.id.uploadTitle);
            progressBar = (ProgressBar) itemView.findViewById(R.id.uploadProgress);

            progressBar.setMax(100);
            progressBar.setProgress(0);

            uploadTitle.setText(getString(R.string.upload_progress, filename));
        }
    }

    @Override
    public void onDateSelected(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        try {
            selectedYear = year;
            selectedMonth = monthOfYear;
            selectedDay = dayOfMonth;

            if(txtTempForDate != null){
                String yearString = "",monthString = "",dayString = "";
                if(selectedYear < 10){
                    yearString += "0" + String.valueOf(selectedYear);
                }else {
                    yearString += String.valueOf(selectedYear);
                }
                if(selectedMonth < 10){
                    monthString += "0" + String.valueOf(selectedMonth+1);
                }else {
                    monthString += String.valueOf(selectedMonth+1);
                }
                if(selectedDay < 10){
                    dayString += "0" + String.valueOf(selectedDay);
                }else {
                    dayString += String.valueOf(selectedDay);
                }

                txtTempForDate.setText(utils.getFormattedDate(yearString + ":" + monthString + ":" + dayString,AppConstants.DATE_FORMAT_FIVE,AppConstants.DATE_FORMAT_SIX));
            }
        }catch (Exception ex){
            Toast.makeText(getActivity(),ex.getMessage(),Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }
}




















