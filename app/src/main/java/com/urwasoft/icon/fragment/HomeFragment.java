package com.urwasoft.icon.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.urwasoft.icon.R;
import com.urwasoft.icon.activity.SurveyDetailActivity;
import com.urwasoft.icon.rest.RestClient;
import com.urwasoft.icon.rest.response.ErrorResponse2;
import com.urwasoft.icon.rest.response.SurveyResponse;
import com.urwasoft.icon.util.AppConstants;
import com.urwasoft.icon.util.SessionManager;
import com.urwasoft.icon.util.SnackUtil;
import com.urwasoft.icon.util.Utils;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {
    private RecyclerView recyclerViewSurveys;
    private SurveyAdapter adapter;
    private Utils utils;
    private SnackUtil snackUtil;
    private AVLoadingIndicatorView avloadingIndicatorView;
    private LinearLayout linearHomeRetry;
    private Button btnRetry;
    private TextView txtNoDataFound;

    private static final int SURVEY_DETAIL_REQUEST_CODE = 123;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        initializeViews(v);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        utils = new Utils(getActivity());
        /*if(utils.onLoadDataInitialize(recyclerViewSurveys, avloadingIndicatorView, linearHomeRetry)){
            callGetClaims(SessionManager.get(getActivity()).getUserId(), AppConstants.TOKEN_ONE,AppConstants.TOKEN_TWO);
        }*/
    }

    private void initializeViews(View v){
        recyclerViewSurveys = (RecyclerView) v.findViewById(R.id.recyclerViewSurveys);
        recyclerViewSurveys.setLayoutManager(new LinearLayoutManager(getActivity()));
        avloadingIndicatorView = (AVLoadingIndicatorView) v.findViewById(R.id.avloadingIndicatorView);
        linearHomeRetry = (LinearLayout) v.findViewById(R.id.linearHomeRetry);
        txtNoDataFound = (TextView) v.findViewById(R.id.txtNoDataFound);
        btnRetry = (Button) v.findViewById(R.id.btnRetry);

        utils = new Utils(getActivity());
        snackUtil = new SnackUtil(getActivity());

        if(utils.onLoadDataInitialize(recyclerViewSurveys, avloadingIndicatorView, linearHomeRetry)){
            callGetClaims(SessionManager.get(getActivity()).getUserId(), AppConstants.TOKEN_ONE,AppConstants.TOKEN_TWO);
        }
        btnRetry.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                utils.onLoadDataInitialize(recyclerViewSurveys, avloadingIndicatorView, linearHomeRetry);
                callGetClaims(SessionManager.get(getActivity()).getUserId(), AppConstants.TOKEN_ONE,AppConstants.TOKEN_TWO);
            }
        });
    }

    private void setupSurveyAdapter(List<SurveyResponse> surveys){
        adapter = new SurveyAdapter(surveys);
        recyclerViewSurveys.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

        }
    }



    private class SurveyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        private LayoutInflater inflater;
        private List<SurveyResponse> surveys;

        public SurveyAdapter(List<SurveyResponse> surveys){
            this.inflater = LayoutInflater.from(getActivity());
            this.surveys = surveys;
        }

        @Override
        public SurveyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = this.inflater.inflate(R.layout.survey_item,parent,false);
            SurveyViewHolder surveyViewHolder = new SurveyViewHolder(v);
            return surveyViewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            SurveyResponse survey = this.surveys.get(position);
            if(holder instanceof SurveyViewHolder){
                SurveyViewHolder surveyViewHolder = (SurveyViewHolder) holder;
                if((survey.getMake() != null) && (!(survey.getMake().equals("")))) {
                    surveyViewHolder.txtMakeModel.setText(survey.getMake());
                }else {
                    surveyViewHolder.txtMakeModel.setText("-");
                }
                surveyViewHolder.txtMakeModel.append("/");
                if((survey.getModel() != null) && (!(survey.getModel().equals("")))) {
                    surveyViewHolder.txtMakeModel.append(survey.getModel());
                }else {
                    surveyViewHolder.txtMakeModel.append("-");
                }
                if((survey.getRegistrationNumber() != null) && (!(survey.getRegistrationNumber().equals("")))) {
                    surveyViewHolder.txtReferenceNo.setText(survey.getOurRef());
                }else {
                    surveyViewHolder.txtReferenceNo.setText("-");
                }
                if((survey.getRegistrationNumber() != null) && (!(survey.getRegistrationNumber().equals("")))) {
                    surveyViewHolder.txtRegNo.setText(survey.getRegistrationNumber());
                }else {
                    surveyViewHolder.txtRegNo.setText("-");
                }
                if((survey.getInsuranceCompanyName() != null) && (!(survey.getInsuranceCompanyName().equals("")))) {
                    surveyViewHolder.txtCompany.setText(survey.getInsuranceCompanyName());
                }else {
                    surveyViewHolder.txtCompany.setText("-");
                }
                if((survey.getPolicyNumber() != null) && (!(survey.getPolicyNumber().equals("")))) {
                    surveyViewHolder.txtPolicyNo.setText(survey.getPolicyNumber());
                }else {
                    surveyViewHolder.txtPolicyNo.setText("-");
                }
                if((survey.getRepairer() != null) && (!(survey.getRepairer().equals("")))) {
                    surveyViewHolder.txtRepairer.setText(survey.getRepairer());
                }else {
                    surveyViewHolder.txtRepairer.setText("-");
                }
                if((survey.getContactForSurvey() != null) && (!(survey.getContactForSurvey().equals("")))) {
                    surveyViewHolder.txtContactForSurvey.setText(survey.getContactForSurvey());
                }else {
                    surveyViewHolder.txtContactForSurvey.setText("-");
                }
            }
        }

        @Override
        public int getItemCount() {
            return surveys.size();
        }

        public void updateSurvey(SurveyResponse survey){
            for(int i=0;i<surveys.size();i++){
                if(surveys.get(i).getOurRef().equals(survey.getOurRef())){
                    surveys.set(i,survey);
                    adapter.notifyItemChanged(i);
                    break;
                }
            }
        }

        public class SurveyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            private TextView txtRegNo,txtCompany,txtPolicyNo,txtRepairer,txtMakeModel,txtContactForSurvey,txtReferenceNo;

            public SurveyViewHolder(View itemView) {
                super(itemView);
                txtReferenceNo = (TextView) itemView.findViewById(R.id.txtReferenceNo);
                txtMakeModel = (TextView) itemView.findViewById(R.id.txtMakeModel);
                txtRegNo = (TextView) itemView.findViewById(R.id.txtRegNo);
                txtCompany = (TextView) itemView.findViewById(R.id.txtCompany);
                txtPolicyNo = (TextView) itemView.findViewById(R.id.txtPolicyNo);
                txtRepairer = (TextView) itemView.findViewById(R.id.txtRepairer);
                txtContactForSurvey = (TextView) itemView.findViewById(R.id.txtContactForSurvey);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SurveyDetailActivity.class);
                intent.putExtra(SurveyDetailActivity.SURVEY_RESPONSE,surveys.get(getLayoutPosition()));
                startActivityForResult(intent,SURVEY_DETAIL_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null){
            if(resultCode == Activity.RESULT_OK){
                if(requestCode == SURVEY_DETAIL_REQUEST_CODE){
                    SurveyResponse surveyResponse = data.getParcelableExtra(SurveyDetailActivity.SURVEY_RESPONSE);
                    boolean isSurveyUpdated = data.getBooleanExtra(SurveyDetailActivity.IS_SURVEY_UPDATED,false);
                    if(isSurveyUpdated) {
                        adapter.updateSurvey(surveyResponse);
                    }
                }
            }
        }
    }

    public void callGetClaims(int userId, String token1, String token2){
        Call<List<SurveyResponse>> call = RestClient.get().getApiService().getClaims(userId,token1,token2);
        call.enqueue(new Callback<List<SurveyResponse>>() {
            @Override
            public void onResponse(Call<List<SurveyResponse>> call, Response<List<SurveyResponse>> response) {
                if(getActivity() != null){
                    //progressCustomDialogControllerSigningIn.hideDialog();
                    if(response.isSuccessful()){
                        List<SurveyResponse> surveys = response.body();
                        if(surveys != null){
                            if(surveys.size() != 0){
                                txtNoDataFound.setVisibility(View.GONE);
                                utils.onDataReceived(recyclerViewSurveys, avloadingIndicatorView, linearHomeRetry);
                                setupSurveyAdapter(surveys);
                            }else {
                                // list is empty
                                utils.onEmptyData(recyclerViewSurveys, avloadingIndicatorView, linearHomeRetry);
                                txtNoDataFound.setVisibility(View.VISIBLE);
                                return;
                            }
                        }else {
                            // list is null
                            utils.onEmptyData(recyclerViewSurveys, avloadingIndicatorView, linearHomeRetry);
                            txtNoDataFound.setVisibility(View.VISIBLE);
                            return;
                        }
                    }else {
                        ErrorResponse2 errorResponse = parseError(response);
                        if(errorResponse != null) {
                            if(errorResponse.getMessage() != null) {
                                utils.onErrorReceived(recyclerViewSurveys, avloadingIndicatorView, linearHomeRetry,
                                        errorResponse.getMessage());
                                //snackUtil.showSnackBarLongTime(recyclerViewSurveys, errorResponse.getMessage());
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<SurveyResponse>> call, Throwable t) {
                if(getActivity() != null){
                    if (!(utils.isInternetAvailable())) {
                        utils.onErrorReceived(recyclerViewSurveys, avloadingIndicatorView, linearHomeRetry,
                                utils.getStringFromResourceId(R.string.please_check_internet_connection));
                    } else {
                        utils.onErrorReceived(recyclerViewSurveys, avloadingIndicatorView, linearHomeRetry,
                                utils.getStringFromResourceId(R.string.something_went_wrong));
                    }
                    t.printStackTrace();
                }
            }
        });
    }

    public ErrorResponse2 parseError(Response<?> response) {
        Converter<ResponseBody, ErrorResponse2> converter =
                RestClient.get().getRetrofit()
                        .responseBodyConverter(ErrorResponse2.class, new Annotation[0]);
        ErrorResponse2 error;
        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ErrorResponse2();
        }

        return error;
    }
}



















































