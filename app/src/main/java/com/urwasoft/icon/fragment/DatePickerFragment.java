package com.urwasoft.icon.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.urwasoft.icon.R;

import java.util.Calendar;

/**
 * Created by Admin on 3/7/2017.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private DateSelectedListener dateSelectedListener;
    private int selectedYear,selectedMonth,selectedDay;

    public DatePickerFragment(){

    }

    @SuppressLint("ValidFragment")
    public DatePickerFragment(DateSelectedListener dateSelectedListener,int selectedYear,int selectedMonth,int selectedDay){
        this.dateSelectedListener = dateSelectedListener;
        this.selectedYear = selectedYear;
        this.selectedMonth = selectedMonth;
        this.selectedDay = selectedDay;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        //Use the current time as the default values for the time picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int dayOnMonth = c.get(Calendar.DAY_OF_MONTH);

        if( (selectedYear == 0) && (selectedMonth == 0) && (selectedDay == 0) ){
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme,this,year,month,dayOnMonth);
            //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

            return datePickerDialog;
            //return new DatePickerDialog(getActivity(),R.style.DialogTheme,this,year,month,dayOnMonth);
        }else {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),R.style.DialogTheme,this,selectedYear,selectedMonth,selectedDay);
            //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return datePickerDialog;
            //return new DatePickerDialog(getActivity(),R.style.DialogTheme,this,selectedYear,selectedMonth,selectedDay);
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        this.dateSelectedListener.onDateSelected(datePicker,year,monthOfYear,dayOfMonth);
    }

    public interface DateSelectedListener {
        void onDateSelected(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth);
    }

}













