package com.urwasoft.icon.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.urwasoft.icon.R;
import com.urwasoft.icon.activity.HomeActivity;
import com.urwasoft.icon.controller.ProgressCustomDialogController;
import com.urwasoft.icon.rest.RestClient;
import com.urwasoft.icon.rest.response.ErrorResponse2;
import com.urwasoft.icon.rest.response.UserResponse;
import com.urwasoft.icon.util.AppConstants;
import com.urwasoft.icon.util.SessionManager;
import com.urwasoft.icon.util.SnackUtil;
import com.urwasoft.icon.util.Utils;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends Fragment implements View.OnClickListener{
    private TextInputEditText edtUsername,edtPassword;
    private Button btnSignIn;
    private Utils utils;
    private SnackUtil snackUtil;
    private ProgressCustomDialogController progressCustomDialogControllerSigningIn;
    private ImageView imgUrwasoft;

    public SignInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_sign_in, container, false);

        initializeViews(v);

        return v;
    }

    private void initializeViews(View v){
        edtUsername = (TextInputEditText) v.findViewById(R.id.edtUsername);
        edtPassword = (TextInputEditText) v.findViewById(R.id.edtPassword);
        btnSignIn = (Button) v.findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(this);
        imgUrwasoft = (ImageView) v.findViewById(R.id.imgUrwasoft);
        imgUrwasoft.setOnClickListener(this);

        utils = new Utils(getActivity());
        snackUtil = new SnackUtil(getActivity());
        progressCustomDialogControllerSigningIn = new ProgressCustomDialogController(getActivity(),R.string.signing_in);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSignIn:{
                if(validateInputField()){
                    if(utils.isInternetAvailable()){
                        callSignIn(edtUsername.getText().toString(),edtPassword.getText().toString(), AppConstants.TOKEN_ONE,AppConstants.TOKEN_TWO);
                        progressCustomDialogControllerSigningIn.showDialog();
                    }else {
                        snackUtil.showSnackBarLongTime(btnSignIn,R.string.please_check_your_internet_connection);
                    }
                }
                break;
            }
            case R.id.imgUrwasoft:{
                openWebPage("http://www.urwasoft.com");
                break;
            }
        }
    }

    private boolean validateInputField(){
        if(utils.isEditTextNullOrEmpty(edtUsername)){
            snackUtil.showSnackBarLongTime(btnSignIn,R.string.username_cannot_be_empty);
            return false;
        }
        if(utils.isEditTextNullOrEmpty(edtPassword)){
            snackUtil.showSnackBarLongTime(btnSignIn,R.string.password_cannot_be_empty);
            return false;
        }
        return true;
    }

    public void callSignIn(String username,String password,String token1,String token2){
        Call<UserResponse> call = RestClient.get().getApiService().signIn(username,password,token1,token2);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(getActivity() != null){
                    progressCustomDialogControllerSigningIn.hideDialog();
                    if(response.isSuccessful()){
                        UserResponse userResponse = response.body();
                        if(userResponse != null){
                            SessionManager.get(getActivity()).createLoginSession(
                                    userResponse.getIdx(),
                                    userResponse.getUsername(),
                                    userResponse.getPassword(),
                                    userResponse.getEmpIdx(),
                                    userResponse.getFullName(),
                                    userResponse.getEmail(),
                                    userResponse.getDepartmentname(),
                                    new Gson().toJson(userResponse));
                            Intent intent = new Intent(getActivity(), HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }else {
                        ErrorResponse2 errorResponse = parseError(response);
                        if(errorResponse != null) {
                            if(errorResponse.getMessage() != null) {
                                snackUtil.showSnackBarLongTime(btnSignIn, errorResponse.getMessage());
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                if(getActivity() != null){
                    progressCustomDialogControllerSigningIn.hideDialog();
                    t.printStackTrace();
                }
            }
        });
    }

    public ErrorResponse2 parseError(Response<?> response) {
        Converter<ResponseBody, ErrorResponse2> converter =
                RestClient.get().getRetrofit()
                        .responseBodyConverter(ErrorResponse2.class, new Annotation[0]);
        ErrorResponse2 error;
        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ErrorResponse2();
        }

        return error;
    }

    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
























