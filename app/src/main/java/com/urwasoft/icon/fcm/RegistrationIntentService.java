package com.urwasoft.icon.fcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.urwasoft.icon.R;
import com.urwasoft.icon.controller.GCMController;
import com.urwasoft.icon.rest.INetwork;
import com.urwasoft.icon.rest.request.AccessToken;
import com.urwasoft.icon.rest.request.GCMTokenRequest;
import com.urwasoft.icon.rest.response.APIResponse;
import com.urwasoft.icon.rest.response.ErrorResponse;
import com.urwasoft.icon.rest.response.UserResponse;
import com.urwasoft.icon.util.AppConstants;
import com.urwasoft.icon.util.SessionManager;
import com.urwasoft.icon.util.ToastUtil;
import com.urwasoft.icon.util.Utils;

import retrofit2.Response;

/**
 * Created by Shahrukh Malik on 3/18/2016.
 */
public class RegistrationIntentService extends IntentService implements INetwork<UserResponse> {
    // abbreviated tag name
    private GCMController gcmController;
    private Utils utils;
    private ToastUtil toastUtil;

    private static final String TAG = "RegIntentServicee";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServerr";
    public static final String GCM_TOKEN = "gcmTokenn";

    public RegistrationIntentService() {
        super(TAG);
        AccessToken accessToken = new AccessToken(AppConstants.BEARER, SessionManager.get(this).getAccessToken());
        gcmController = new GCMController(this, accessToken);
        utils = new Utils(this);
        toastUtil = new ToastUtil(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sendRegistrationToServer(SessionManager.get(this).getFCMToken());
    }

    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
        GCMTokenRequest gcmTokenRequest = new GCMTokenRequest();
        gcmTokenRequest.setGcmToken(token);
        gcmController.callUpdateGCMToken(gcmTokenRequest, "en");

        // if registration sent was successful, store a boolean that indicates whether the generated token has been sent to server
        //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //sharedPreferences.edit().putBoolean(SENT_TOKEN_TO_SERVER, true).apply();
    }

    @Override
    public void onResponse(Response<APIResponse<UserResponse>> response) {
        if(response.body().getStatus()){
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            sharedPreferences.edit().putBoolean(SENT_TOKEN_TO_SERVER, true).apply();
        }
    }

    @Override
    public void onError(Response<APIResponse<UserResponse>> response) {
        APIResponse<UserResponse> apiResponse = gcmController.parseError(response);
        ErrorResponse errorResponse = apiResponse.getErrorResponse();
        toastUtil.showToastLongTime(
                String.valueOf(/*errorResponse.getCustomCode()) + " : " + */errorResponse.getMessage().get(0))
        );
        if(errorResponse.getCustomCode() == AppConstants.STATUS_UNAUTHORIZED){
            //SessionManager.get(this).logoutUser();
        }
    }

    @Override
    public void onNullResponse() {
        toastUtil.showToastLongTime(utils.getStringFromResourceId(R.string.response_null));
    }

    @Override
    public void onFailure(Throwable t) {
        t.printStackTrace();
    }
}
