package com.urwasoft.icon.fcm;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Admin on 8/2/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService{
    private static final String TAG = "MyFirebaseMsgServicee";
    public static final int MESSAGE_NOTIFICATION_ID = 4353455;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        try {
            String title = remoteMessage.getData().get("title").toString();
            String message = remoteMessage.getData().get("message").toString();
            //createNotification(title, message);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /*private void createNotification(String title,String message) {
        Context context = getBaseContext();
        if(context != null) {
            Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(getNotificationIcon())
                    .setContentTitle(title)
                    .setColor(ContextCompat.getColor(this,R.color.colorPrimary))
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setContentText(message);
            NotificationManager mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, mBuilder.build());
        }
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.icon_notification_lollipop : R.drawable.icon_notification_pre_lollipop;
    }*/
}
