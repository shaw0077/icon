package com.urwasoft.icon.fcm;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.urwasoft.icon.R;
import com.urwasoft.icon.controller.GCMController;
import com.urwasoft.icon.rest.INetwork;
import com.urwasoft.icon.rest.request.AccessToken;
import com.urwasoft.icon.rest.request.GCMTokenRequest;
import com.urwasoft.icon.rest.response.APIResponse;
import com.urwasoft.icon.rest.response.ErrorResponse;
import com.urwasoft.icon.rest.response.UserResponse;
import com.urwasoft.icon.util.AppConstants;
import com.urwasoft.icon.util.SessionManager;
import com.urwasoft.icon.util.ToastUtil;
import com.urwasoft.icon.util.Utils;

import retrofit2.Response;

/**
 * Created by Admin on 8/2/2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService  implements INetwork<UserResponse> {
    private static final String TAG = "MyFirebaseIIDServicee";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServerr";
    private GCMController gcmController;
    private Utils utils;
    private ToastUtil toastUtil;

    public MyFirebaseInstanceIDService(){
        AccessToken accessToken = new AccessToken(AppConstants.BEARER, SessionManager.get(this).getAccessToken());
        gcmController = new GCMController(this, accessToken);
        utils = new Utils(this);
        toastUtil = new ToastUtil(this);
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(refreshedToken);
        SessionManager.get(this).setFCMToken(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        GCMTokenRequest gcmTokenRequest = new GCMTokenRequest();
        gcmTokenRequest.setGcmToken(token);
        gcmController.callUpdateGCMToken(gcmTokenRequest, "en");
    }

    @Override
    public void onResponse(Response<APIResponse<UserResponse>> response) {
        if(response.body().getStatus()){
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            sharedPreferences.edit().putBoolean(SENT_TOKEN_TO_SERVER, true).apply();
        }
    }

    @Override
    public void onError(Response<APIResponse<UserResponse>> response) {
        APIResponse<UserResponse> apiResponse = gcmController.parseError(response);
        ErrorResponse errorResponse = apiResponse.getErrorResponse();
        toastUtil.showToastLongTime(
                String.valueOf(/*errorResponse.getCustomCode()) + " : " + */errorResponse.getMessage().get(0))
        );
        if(errorResponse.getCustomCode() == AppConstants.STATUS_UNAUTHORIZED){
            //SessionManager.get(this).logoutUser();
        }
    }

    @Override
    public void onNullResponse() {
        toastUtil.showToastLongTime(utils.getStringFromResourceId(R.string.response_null));
    }

    @Override
    public void onFailure(Throwable t) {
        t.printStackTrace();
    }
}


















